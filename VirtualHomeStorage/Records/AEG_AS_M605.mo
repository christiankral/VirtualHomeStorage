within VirtualHomeStorage.Records;
record AEG_AS_M605 "310W PVSOL module"
  extends PhotoVoltaics.Records.ModuleData(
    final moduleName="310W PVSPOL module",
    final TRef=298.15,
    final irradianceRef=1000,
    final VocRef=40.1,
    final IscRef=10.04,
    final VmpRef=32.6,
    final ImpRef=9.51,
    final alphaIsc=+0.06,
    final alphaVoc=-0.31,
    final ns=60,
    final nb=3);
  annotation (
    defaultComponentName = "moduleData",
    defaultComponentPrefixes = "parameter",
    Documentation(info = "<html>
The original data of this module are taken from
<a href=\"http://www.elektra.si/uploads/datoteke/trina_tsm-195-200-205-210dc80.08_mono.pdf\">Comax</a>. You may want to download this PDF file and store it in the directory Resources/DataSheets for convenience reasons. You may want to make these data directly 
<a href=\"modelica://PhotoVoltaics/Resources/DataSheets/TSM_200_DC01A.pdf\">available</a>.
</html>"));
end AEG_AS_M605;
