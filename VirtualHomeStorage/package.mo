within ;
package VirtualHomeStorage "Modelica library for the investigation of real and virtual electrical home storages in home utilizing photovoltaics"
  extends Modelica.Icons.Package;

  annotation (uses(
    Modelica(version="4.0.0"),
    PhotoVoltaics(version="2.0.0"),
    Complex(version="4.0.0")));
end VirtualHomeStorage;
