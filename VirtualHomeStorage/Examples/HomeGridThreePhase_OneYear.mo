within VirtualHomeStorage.Examples;
model HomeGridThreePhase_OneYear "One year based on real weather data; Comax modules at TGM building in Vienna, Austria"
  extends Modelica.Icons.Example;
  extends Modelica.Icons.UnderConstruction;
  parameter Integer nsModule = 1 "Number of series connected modules";
  parameter Integer npModule = 1 "Number of parallel connected modules";
  parameter String fileName = Modelica.Utilities.Files.loadResource("modelica://VirtualHomeStorage/Resources/Irradiance/vienna.txt") "File name";
  parameter String csvFileName = "energy.csv";
  PhotoVoltaics.Components.SimplePhotoVoltaics.SimplePlantSymmetric plant(
    useConstantIrradiance=false,
    moduleData=moduleData,
    npModule=npModule,
    nsModule=nsModule,
    shadow=0.1) annotation (Placement(visible=true, transformation(
        origin={-50,-10},
        extent={{10,-10},{-10,10}},
        rotation=90)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (
    Placement(visible = true, transformation(origin={-50,-40},    extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor annotation (
    Placement(transformation(extent={{-30,0},{-10,20}})));
  Modelica.Blocks.Continuous.Integrator integrator(y(unit = "J")) annotation (
    Placement(transformation(extent={{-50,-80},{-70,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable vienna(
    tableOnFile=true,
    tableName="Vienna",
    fileName=fileName,
    columns=2:10)                                               "Reads irradiance from file" annotation (Placement(transformation(extent={{-100,-20},{-80,0}})));
  parameter Records.AEG_AS_M605 moduleData annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  PhotoVoltaics.Components.Blocks.MPTrackerSample mpTracker(VmpRef=moduleData.VmpRef, ImpRef=moduleData.ImpRef)     annotation (
    Placement(transformation(extent={{-20,-80},{0,-60}})));
  Converter.Components.QuasiStaticMultiPhaseConverter                converter annotation (Placement(transformation(extent={{0,-20},{20,0}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Sources.VoltageSource voltageSource(
    f=50,
    V=fill(400/sqrt(3), 3),
    gamma(fixed=true, start=0)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={70,-10})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star star annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={70,-40})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground groundAC
    annotation (Placement(transformation(extent={{60,-80},{80,-60}})));
  Loads.MultiPhaseLoad multiPhaseLoad annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={50,-10})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Resistor resistor(R_ref=fill(
        1E-3, 3)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,-30})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star star1 annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,-56})));
equation
  connect(ground.p, plant.n) annotation (
    Line(points={{-50,-30},{-50,-20}},      color = {0, 0, 255}));
  connect(powerSensor.pc, powerSensor.pv) annotation (
    Line(points={{-30,10},{-30,20},{-20,20}},        color = {0, 0, 255}));
  connect(plant.p, powerSensor.pc) annotation (
    Line(points={{-50,0},{-50,10},{-30,10}},        color = {0, 0, 255}));
  connect(powerSensor.nv, ground.p) annotation (
    Line(points={{-20,0},{-20,-30},{-50,-30}},                              color = {0, 0, 255}));
  connect(integrator.u, powerSensor.power) annotation (
    Line(points={{-48,-70},{-30,-70},{-30,-1}},                    color = {0, 0, 127}));
  connect(mpTracker.vRef,converter. vDCRef) annotation (
    Line(points={{1,-70},{4,-70},{4,-22}},           color = {0, 0, 127}));
  connect(converter.ac,voltageSource.plug_p) annotation (
    Line(points={{20,-10},{24,-10},{24,10},{70,10},{70,0}},                           color = {85, 170, 255}));
  connect(star.plug_p,voltageSource.plug_n) annotation (
    Line(points={{70,-30},{70,-20}},                 color = {85, 170, 255}));
  connect(star.pin_n,groundAC. pin) annotation (
    Line(points={{70,-50},{70,-60}},                 color = {85, 170, 255}));
  connect(powerSensor.nc, converter.dc_p) annotation (Line(points={{-10,10},{-10,-4},{0,-4}},color={0,0,255}));
  connect(ground.p, converter.dc_n) annotation (Line(points={{-50,-30},{-10,-30},{-10,-16},{0,-16}},color={0,0,255}));
  connect(mpTracker.power, powerSensor.power) annotation (Line(points={{-22,-70},{-30,-70},{-30,-1}},color={0,0,127}));
  connect(converter.ac, multiPhaseLoad.plug_p) annotation (Line(points={{20,-10},{24,-10},{24,10},{50,10},{50,0}},
                                                                                            color={85,170,255}));
  connect(vienna.y[4], plant.variableIrradiance) annotation (Line(points={{-79,-10},{-62,-10}}, color={0,0,127}));
  connect(vienna.y[7], multiPhaseLoad.power) annotation (Line(points={{-79,-10},{-70,-10},{-70,30},{30,30},{30,-10},{38,-10}}, color={0,0,127}));
  connect(resistor.plug_p, multiPhaseLoad.plug_n) annotation (Line(points={{30,-20},{50,-20}}, color={85,170,255}));
  connect(star1.plug_p, resistor.plug_n) annotation (Line(points={{30,-46},{30,-40}}, color={85,170,255}));
  annotation (
    experiment(
      StopTime = 3.1536e+07,
      Interval=900,
      Tolerance= 1e-08, StartTime = 0),
    Documentation(revisions = "<html>
</html>",
        info="<html>
<p>This example is based on weather data taken from 
<a href=\"https://energyplus.net/weather-location/europe_wmo_region_6/AUT//AUT_Vienna.Schwechat.110360_IWEC\">https://energyplus.net</a>. 
The EPW file was converted using a Java script provided by the 
<a href=\"https://github.com/lbl-srg/modelica-buildings\">Buildings</a> library.</p>
<p><code>
java -jar .../Buildings/Resources/bin/ConvertWeatherData.jar .../PhotoVoltaics/Resources/WeatherData/AUT_Vienna.Schwechat.110360_IWEC.epw
</code></p>
<p>The weather data are distributed under the 
<a href=\"https://energyplus.net/licensing\">EnergyPlus Licensing</a> conditions, see 
<a href=\"PhotoVoltaics.UsersGuide.License\">License</a>.
</p>
</html>"));
end HomeGridThreePhase_OneYear;