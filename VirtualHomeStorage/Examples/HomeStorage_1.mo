within VirtualHomeStorage.Examples;
model HomeStorage_1 "Home storage with PV, load and battery"
  extends Modelica.Icons.Example;
  parameter Integer nsModule = 11 "Number of series connected modules";
  parameter Integer npModule = 1 "Number of parallel connected modules";
  parameter String fileName = Modelica.Utilities.Files.loadResource("modelica://VirtualHomeStorage/Resources/Irradiance/vienna.txt") "File name";
  Modelica.Electrical.QuasiStatic.SinglePhase.Sources.VoltageSource
    voltageSource(
    V=230,
    f=50,
    gamma(fixed=true, start=0),
    phi=0) annotation (Placement(visible=true, transformation(
        origin={130,30},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Blocks.Sources.CombiTimeTable vienna(columns = 2:10, fileName = fileName, smoothness = Modelica.Blocks.Types.Smoothness.LinearSegments, tableName = "Vienna", tableOnFile = true, startTime = 0) annotation (
    Placement(visible = true, transformation(extent = {{-150, 20}, {-130, 40}}, rotation = 0)));
  parameter VirtualHomeStorage.Records.AEG_AS_M605 moduleData annotation (
    Placement(visible = true, transformation(origin = {-90, 122}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  VirtualHomeStorage.Loads.SinglePhaseLoad load annotation (
    Placement(visible = true, transformation(origin = {52, 30}, extent = {{-10, 10}, {10, -10}}, rotation = 270)));
  VirtualHomeStorage.Sensors.EnergySensorsSinglePhase energyMeterTotal annotation (
    Placement(visible = true, transformation(origin = {80, 50}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  VirtualHomeStorage.Controller.ChargeController chargeController(SOCmax = 0.98, SOCmin = 0.02, SOCslope = 0.02, dSOC = 1E-3, powerMax = 5000, powerMin = -1) annotation (
    Placement(visible = true, transformation(origin = {-38, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Converter.QuasiStaticSinglePhaseLossConverterControlled converterBattery(PRef = 3000, table = [0, 0; 7000, 0]) annotation (
    Placement(visible = true, transformation(extent = {{-30, -50}, {-10, -30}}, rotation = 0)));
  Modelica.Electrical.Batteries.BatteryStacks.CellStack battery(Np = 2, Ns = 15, SOC(fixed = true, start = 0.5), cellData = cellData) annotation (
    Placement(visible = true, transformation(origin = {-78, -50}, extent = {{-10, 10}, {10, -10}}, rotation = 270)));
  parameter Modelica.Electrical.Batteries.ParameterRecords.CellData cellData(Idis = 0, OCV_SOC = [0.0, 0.7704615384615384; 0.05, 0.8123076923076923; 0.1, 0.832; 0.15, 0.8467692307692307; 0.2, 0.8590769230769232; 0.25, 0.8664615384615385; 0.3, 0.8726153846153846; 0.35, 0.8775384615384615; 0.4, 0.8836923076923077; 0.45, 0.889846153846154; 0.5, 0.8955076923076923; 0.55, 0.9016615384615384; 0.6, 0.9078153846153847; 0.65, 0.9139692307692308; 0.7, 0.9201230769230769; 0.75, 0.926276923076923; 0.8, 0.9324307692307692; 0.85, 0.9385846153846155; 0.9, 0.9476923076923077; 0.95, 0.96; 1.0, 1.0], OCVmax = 4.0625, OCVmin = 3.13, Qnom = 90000, Ri = 0.023, SOCmax = 1, SOCmin = 0, alpha = 0, smoothness = Modelica.Blocks.Types.Smoothness.ContinuousDerivative, useLinearSOCDependency = false) annotation (
    Placement(visible = true, transformation(extent = {{-60, 112}, {-40, 132}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground groundBattery annotation (
    Placement(visible = true, transformation(origin = {-78, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground groundPV annotation (
    Placement(visible = true, transformation(origin = {-80, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PhotoVoltaics.Components.SimplePhotoVoltaics.SimplePlantSymmetric plantPV(moduleData = moduleData, T = 298.15, useConstantIrradiance = false, nsModule = nsModule, npModule = npModule) annotation (
    Placement(visible = true, transformation(origin = {-80, 30}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
  PhotoVoltaics.Components.Converters.QuasiStaticSinglePhaseConverter converterPV annotation (
    Placement(transformation(extent = {{-30, 20}, {-10, 40}})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensorPV annotation (
    Placement(transformation(extent = {{-60, 42}, {-40, 62}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground groundAC annotation (
     Placement(visible=true, transformation(extent={{120,-12},{140,8}},
          rotation=0)));
  Modelica.Blocks.Tables.CombiTable1Ds combiTable(final table = [0, 0; 50, 285.738; 100, 303.671; 150, 314.068; 200, 321.619; 250, 327.707; 300, 332.56; 350, 336.437; 400, 340.039; 450, 343.091; 500, 345.82; 550, 348.386; 600, 350.664; 650, 352.592; 700, 354.533; 750, 356.385; 800, 358.138; 850, 359.79; 900, 361.254; 950, 362.708; 1000, 364.047]) annotation (
    Placement(transformation(extent = {{-60, -18}, {-40, 2}})));
  Modelica.Blocks.Continuous.LimIntegrator integrator(k = -0.01, outMax = 2000) annotation (
    Placement(visible = true, transformation(extent = {{60, -100}, {40, -80}}, rotation = 0)));
  Sensors.ChargeSensor chargeSensor(Qnom = 180000, SOCstart = 0.5) annotation (
    Placement(visible = true, transformation(origin = {-50, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(battery.n, converterBattery.dc_n) annotation (
    Line(points = {{-78, -60}, {-78, -70}, {-68, -70}, {-68, -46}, {-30, -46}}, color = {0, 0, 255}));
  connect(groundBattery.p, battery.n) annotation (
    Line(points = {{-78, -80}, {-78, -60}}, color = {0, 0, 255}));
  connect(converterBattery.powerRef, chargeController.powerBattery) annotation (
    Line(points = {{-14, -52}, {-14, -70}, {-27, -70}}, color = {0, 0, 127}));
  connect(energyMeterTotal.pv, energyMeterTotal.pc) annotation (
    Line(points = {{80, 60}, {90, 60}, {90, 50}}, color = {85, 170, 255}));
  connect(energyMeterTotal.nc, load.pin_p) annotation (
    Line(points = {{70, 50}, {52, 50}, {52, 40}}, color = {85, 170, 255}));
  connect(energyMeterTotal.pc, voltageSource.pin_p) annotation (
    Line(points = {{90, 50}, {130, 50}, {130, 40}}, color = {85, 170, 255}));
  connect(vienna.y[7], load.power) annotation (
    Line(points = {{-129, 30}, {-110, 30}, {-110, 90}, {20, 90}, {20, 30}, {40, 30}}, color = {0, 0, 127}));
  connect(groundPV.p, plantPV.n) annotation (
    Line(points = {{-80, 10}, {-80, 20}}, color = {0, 0, 255}));
  connect(groundPV.p, converterPV.dc_n) annotation (
    Line(points = {{-80, 10}, {-40, 10}, {-40, 24}, {-30, 24}}, color = {0, 0, 255}));
  connect(plantPV.p, powerSensorPV.pc) annotation (
    Line(points = {{-80, 40}, {-80, 52}, {-60, 52}}, color = {0, 0, 255}));
  connect(powerSensorPV.nc, converterPV.dc_p) annotation (
    Line(points = {{-40, 52}, {-40, 36}, {-30, 36}}, color = {0, 0, 255}));
  connect(powerSensorPV.pc, powerSensorPV.pv) annotation (
    Line(points = {{-60, 52}, {-60, 62}, {-50, 62}}, color = {0, 0, 255}));
  connect(powerSensorPV.nv, groundPV.p) annotation (
    Line(points = {{-50, 42}, {-50, 10}, {-80, 10}}, color = {0, 0, 255}));
  connect(converterPV.ac_p, converterBattery.ac_p) annotation (
    Line(points = {{-10, 36}, {8, 36}, {8, -34}, {-10, -34}}, color = {85, 170, 255}));
  connect(converterPV.ac_n, converterBattery.ac_n) annotation (
    Line(points = {{-10, 24}, {20, 24}, {20, -46}, {-10, -46}}, color = {85, 170, 255}));
  connect(vienna.y[3], plantPV.variableIrradiance) annotation (
    Line(points = {{-129, 30}, {-92, 30}}, color = {0, 0, 127}));
  connect(voltageSource.pin_n, groundAC.pin) annotation (
    Line(points = {{130, 20}, {130, 8}}, color = {85, 170, 255}));
  connect(energyMeterTotal.nv, groundAC.pin) annotation (
    Line(points = {{80, 40}, {80, 8}, {130, 8}}, color = {85, 170, 255}));
  connect(combiTable.u, vienna.y[1]) annotation (
    Line(points = {{-62, -8}, {-110, -8}, {-110, 30}, {-129, 30}}, color = {0, 0, 127}));
  connect(combiTable.y[1], converterPV.vDCRef) annotation (
    Line(points = {{-39, -8}, {-26, -8}, {-26, 18}}, color = {0, 0, 127}));
  connect(integrator.u, energyMeterTotal.power) annotation (
    Line(points = {{62, -90}, {96, -90}, {96, 44}, {91, 44}}, color = {0, 0, 127}));
  connect(integrator.y, chargeController.powerRef) annotation (
    Line(points = {{39, -90}, {-60, -90}, {-60, -80}, {-50, -80}}, color = {0, 0, 127}));
  connect(converterPV.ac_p, energyMeterTotal.nc) annotation (
    Line(points = {{-10, 36}, {8, 36}, {8, 50}, {70, 50}}, color = {85, 170, 255}));
  connect(converterPV.ac_n, groundAC.pin) annotation (
    Line(points = {{-10, 24}, {20, 24}, {20, 8}, {130, 8}}, color = {85, 170, 255}));
  connect(load.pin_n, groundAC.pin) annotation (
    Line(points = {{52, 20}, {52, 8}, {130, 8}}, color = {85, 170, 255}));
  connect(chargeSensor.nGrid, converterBattery.dc_p) annotation (
    Line(points = {{-40, -30}, {-40, -34}, {-30, -34}}, color = {0, 0, 255}));
  connect(chargeSensor.pBattery, battery.p) annotation (
    Line(points = {{-60, -30}, {-78, -30}, {-78, -40}}, color = {0, 0, 255}));
  connect(chargeSensor.SOC, chargeController.SOC) annotation (
    Line(points = {{-60, -41}, {-60, -60}, {-50, -60}}, color = {0, 0, 127}));
  annotation (
    Diagram(graphics={  Rectangle(origin = {80.625, 36.7308}, fillColor = {255, 225, 207}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                            FillPattern.Solid, extent = {{-20.625, 43.2692}, {19.375, -56.7308}}), Rectangle(origin = {45, 31.923}, fillColor = {209, 255, 183}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-15, 48.077}, {15, -51.923}}), Rectangle(origin = {-112.107, 34.8076}, fillColor = {254, 255, 185}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-47.8929, 45.1924}, {-7.89462, -54.8076}}), Rectangle(origin = {-51.5151, 22.3077}, fillColor = {253, 213, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-48.4849, 57.6923}, {51.5151, -42.3077}}), Rectangle(origin = {-50, -60}, fillColor = {207, 253, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-50, 40}, {50, -40}}), Rectangle(origin = {130.625, 36.7308}, fillColor = {224, 237, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-20.625, 43.2692}, {19.375, -56.7308}})}, coordinateSystem(initialScale = 0.1)),
    experiment(StopTime = 3.1536e+07, Interval = 60, Tolerance = 1e-06, StartTime = 0));
end HomeStorage_1;