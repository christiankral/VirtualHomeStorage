within VirtualHomeStorage.Examples;
model HomeStorage_2Homes "Homes with PV, load and battery"
  extends Modelica.Icons.Example;
  parameter Integer nsModule = 11 "Number of series connected modules";
  parameter Integer npModule = 1 "Number of parallel connected modules";
  parameter String fileName = Modelica.Utilities.Files.loadResource("modelica://VirtualHomeStorage/Resources/Irradiance/vienna.txt") "File name";
  Modelica.Electrical.QuasiStatic.SinglePhase.Sources.VoltageSource
    voltageSource(
    V=230,
    f=50,
    gamma(fixed=true, start=0),
    phi=0) annotation (Placement(visible=true, transformation(
        origin={80,30},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Blocks.Sources.CombiTimeTable vienna(columns = 2:10, fileName = fileName, smoothness = Modelica.Blocks.Types.Smoothness.LinearSegments, startTime = 0, tableName = "Vienna", tableOnFile = true) annotation (
    Placement(visible = true, transformation(extent={{-90,20},{-70,40}},        rotation = 0)));
  parameter VirtualHomeStorage.Records.AEG_AS_M605 moduleData annotation (
    Placement(visible = true, transformation(origin = {-90, 122}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  VirtualHomeStorage.Controller.ChargeController chargeController(dSOC = 1E-4,
    powerMax=20000,                                                                             powerMin = 5E-3) annotation (
    Placement(visible = true, transformation(origin = {-38, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  VirtualHomeStorage.Converter.QuasiStaticSinglePhaseLossConverterControlled converterBattery(PRef = 5000, Tc = 1E-2, Ti = 1E-2,
    table=[0,200; 70000,2000])                                                                                                                               annotation (
    Placement(visible = true, transformation(extent = {{-30, -50}, {-10, -30}}, rotation = 0)));
  Modelica.Electrical.Batteries.BatteryStacks.CellStack battery(
    Np=16,                                                                                        Ns = 15, SOC(fixed = true, start = 0.5), cellData = cellData) annotation (
    Placement(visible = true, transformation(origin = {-78, -50}, extent = {{-10, 10}, {10, -10}}, rotation = 270)));
  parameter Modelica.Electrical.Batteries.ParameterRecords.CellData cellData(Idis = 0, OCV_SOC = [0.0, 0.7704615384615384; 0.05, 0.8123076923076923; 0.1, 0.832; 0.15, 0.8467692307692307; 0.2, 0.8590769230769232; 0.25, 0.8664615384615385; 0.3, 0.8726153846153846; 0.35, 0.8775384615384615; 0.4, 0.8836923076923077; 0.45, 0.889846153846154; 0.5, 0.8955076923076923; 0.55, 0.9016615384615384; 0.6, 0.9078153846153847; 0.65, 0.9139692307692308; 0.7, 0.9201230769230769; 0.75, 0.926276923076923; 0.8, 0.9324307692307692; 0.85, 0.9385846153846155; 0.9, 0.9476923076923077; 0.95, 0.96; 1.0, 1.0], OCVmax = 4.0625, OCVmin = 3.13, Qnom = 90000, Ri = 0.023, SOCmax = 1, SOCmin = 0, alpha = 0, smoothness = Modelica.Blocks.Types.Smoothness.ContinuousDerivative, useLinearSOCDependency = false) annotation (
    Placement(visible = true, transformation(extent = {{-60, 112}, {-40, 132}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground groundBattery annotation (
    Placement(visible = true, transformation(origin = {-78, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground groundAC annotation (
     Placement(visible=true, transformation(extent={{70,-12},{90,8}}, rotation=
            0)));
  Modelica.Blocks.Continuous.LimIntegrator integrator(k = -0.01, outMax=20000)  annotation (
    Placement(visible = true, transformation(extent={{30,-100},{10,-80}},      rotation = 0)));
  Sensors.ChargeSensor chargeSensor(Qnom = 180000, SOCstart = 0.5) annotation (
    Placement(visible = true, transformation(origin = {-50, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Homes.Home home1(gainLoad=1, moduleData=moduleData)
    annotation (Placement(transformation(extent={{10,80},{30,100}})));
  Homes.Home home2(gainLoad=0.9, moduleData=moduleData)
    annotation (Placement(transformation(extent={{10,50},{30,70}})));
  Homes.Home home3(gainLoad=1.1, moduleData=moduleData)
    annotation (Placement(transformation(extent={{10,20},{30,40}})));
  Homes.Home home4(gainLoad=1.2, moduleData=moduleData)
    annotation (Placement(transformation(extent={{10,-8},{30,12}})));
  Modelica.Blocks.Math.Sum sumPower(nin=4)
    annotation (Placement(transformation(extent={{70,-100},{50,-80}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Impedance impedance_p(Z_ref(
        re=0.01, im=0.001))
    annotation (Placement(transformation(extent={{10,-44},{30,-24}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Impedance impedance_n(Z_ref(
        re=0.01, im=0.001))
    annotation (Placement(transformation(extent={{10,-36},{30,-56}})));
equation
  connect(battery.n, converterBattery.dc_n) annotation (
    Line(points = {{-78, -60}, {-78, -70}, {-68, -70}, {-68, -46}, {-30, -46}}, color = {0, 0, 255}));
  connect(groundBattery.p, battery.n) annotation (
    Line(points = {{-78, -80}, {-78, -60}}, color = {0, 0, 255}));
  connect(voltageSource.pin_n, groundAC.pin) annotation (
    Line(points={{80,20},{80,8}},        color = {85, 170, 255}));
  connect(integrator.y, chargeController.powerRef) annotation (
    Line(points={{9,-90},{-60,-90},{-60,-80},{-50,-80}},           color = {0, 0, 127}));
  connect(chargeSensor.nGrid, converterBattery.dc_p) annotation (
    Line(points = {{-40, -30}, {-40, -34}, {-30, -34}}, color = {0, 0, 255}));
  connect(chargeSensor.pBattery, battery.p) annotation (
    Line(points = {{-60, -30}, {-78, -30}, {-78, -40}}, color = {0, 0, 255}));
  connect(chargeSensor.SOC, chargeController.SOC) annotation (
    Line(points = {{-60, -41}, {-60, -60}, {-50, -60}}, color = {0, 0, 127}));
  connect(chargeController.powerBattery, converterBattery.powerRef) annotation (Line(points={{-27,-70},{-14,-70},{-14,-52}}, color={0,0,127}));
  connect(home1.pin_p, voltageSource.pin_p) annotation (Line(points={{30,96},{
          66,96},{66,40},{80,40}}, color={85,170,255}));
  connect(home1.pin_n, voltageSource.pin_n) annotation (Line(points={{30,84},{
          60,84},{60,20},{80,20}}, color={85,170,255}));
  connect(vienna.y[3], home1.variableIrradiance) annotation (Line(points={{-69,
          30},{-20,30},{-20,96},{8,96}}, color={0,0,127}));
  connect(vienna.y[7], home1.loadPower) annotation (Line(points={{-69,30},{-30,
          30},{-30,84},{8,84}}, color={0,0,127}));
  connect(home2.pin_p, voltageSource.pin_p) annotation (Line(points={{30,66},{
          66,66},{66,40},{80,40}}, color={85,170,255}));
  connect(home2.pin_n, voltageSource.pin_n) annotation (Line(points={{30,54},{
          60,54},{60,20},{80,20}}, color={85,170,255}));
  connect(vienna.y[3], home2.variableIrradiance) annotation (Line(points={{-69,
          30},{-20,30},{-20,66},{8,66}}, color={0,0,127}));
  connect(vienna.y[7], home2.loadPower) annotation (Line(points={{-69,30},{-30,
          30},{-30,54},{8,54}}, color={0,0,127}));
  connect(home3.pin_p, voltageSource.pin_p) annotation (Line(points={{30,36},{
          50,36},{50,40},{80,40}}, color={85,170,255}));
  connect(home3.pin_n, voltageSource.pin_n) annotation (Line(points={{30,24},{
          60,24},{60,20},{80,20}}, color={85,170,255}));
  connect(vienna.y[3], home3.variableIrradiance) annotation (Line(points={{-69,
          30},{-20,30},{-20,36},{8,36}}, color={0,0,127}));
  connect(vienna.y[7], home3.loadPower) annotation (Line(points={{-69,30},{-30,
          30},{-30,24},{8,24}}, color={0,0,127}));
  connect(home4.pin_p, voltageSource.pin_p) annotation (Line(points={{30,8},{50,
          8},{50,40},{80,40}}, color={85,170,255}));
  connect(home4.pin_n, voltageSource.pin_n) annotation (Line(points={{30,-4},{
          60,-4},{60,20},{80,20}}, color={85,170,255}));
  connect(vienna.y[3], home4.variableIrradiance) annotation (Line(points={{-69,
          30},{-20,30},{-20,8},{8,8}}, color={0,0,127}));
  connect(vienna.y[7], home4.loadPower) annotation (Line(points={{-69,30},{-30,
          30},{-30,-4},{8,-4}}, color={0,0,127}));
  connect(integrator.u, sumPower.y)
    annotation (Line(points={{32,-90},{49,-90}}, color={0,0,127}));
  connect(home1.power, sumPower.u[1]) annotation (Line(points={{20,79},{20,74},{
          40,74},{40,-60},{96,-60},{96,-90.75},{72,-90.75}},color={0,0,127}));
  connect(home2.power, sumPower.u[2]) annotation (Line(points={{20,49},{20,44},{
          40,44},{40,-60},{96,-60},{96,-90},{84,-90},{84,-90.25},{72,-90.25}},
        color={0,0,127}));
  connect(home3.power, sumPower.u[3]) annotation (Line(points={{20,19},{20,14},{
          40,14},{40,-60},{96,-60},{96,-89.75},{72,-89.75}},color={0,0,127}));
  connect(home4.power, sumPower.u[4]) annotation (Line(points={{20,-9},{20,-14},
          {40,-14},{40,-60},{96,-60},{96,-89.25},{72,-89.25}},
                                                             color={0,0,127}));
  connect(converterBattery.ac_p, impedance_p.pin_p)
    annotation (Line(points={{-10,-34},{10,-34}}, color={85,170,255}));
  connect(converterBattery.ac_n, impedance_n.pin_p)
    annotation (Line(points={{-10,-46},{10,-46}}, color={85,170,255}));
  connect(impedance_p.pin_n, voltageSource.pin_p) annotation (Line(points={{30,
          -34},{50,-34},{50,40},{80,40}}, color={85,170,255}));
  connect(impedance_n.pin_n, voltageSource.pin_n) annotation (Line(points={{30,
          -46},{60,-46},{60,20},{80,20}}, color={85,170,255}));
  annotation (
    Diagram(graphics={                                                                                                                                                                                                        Rectangle(origin={
              -52.107,34.8076},                                                                                                                                                                                                        fillColor = {254, 255, 185}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-47.8929, 45.1924}, {-7.89462, -54.8076}}),                                                                                                                                                                                         Rectangle(origin = {-50, -60}, fillColor = {207, 253, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-50, 40}, {50, -40}}), Rectangle(origin={
              86.4375,36.7308},                                                                                                                                                                                                        fillColor = {224, 237, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent={{
              -14.4375,43.2692},{13.5625,-56.7308}})},                                                                                                                                                                                                        coordinateSystem(initialScale = 0.1)),
    experiment(StopTime = 2.592e+06, Interval = 60, Tolerance = 1e-06, StartTime = 0));
end HomeStorage_2Homes;