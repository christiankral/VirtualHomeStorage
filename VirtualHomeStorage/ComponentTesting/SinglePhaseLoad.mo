within VirtualHomeStorage.ComponentTesting;
model SinglePhaseLoad
  extends Modelica.Icons.Example;
  Loads.SinglePhaseLoad singlePhaseLoad annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={20,0})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground ground
    annotation (Placement(transformation(extent={{-50,-50},{-30,-30}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Sources.VoltageSource
    voltageSource(
    gamma(fixed=true, start=0),
    f=50,
    V=100,
    phi=0) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-40,0})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Sensors.PowerSensor powerSensor
    annotation (Placement(transformation(extent={{-20,10},{0,30}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=500,
    duration=0.5,
    offset=500,
    startTime=0.25) annotation (Placement(transformation(extent={{70,-10},{50,10}})));
equation
  connect(voltageSource.pin_p, powerSensor.currentP) annotation (Line(points={{-40,10},{-40,20},{-20,20}}, color={85,170,255}));
  connect(powerSensor.currentN, singlePhaseLoad.pin_p) annotation (Line(points={{0,20},{20,20},{20,10}}, color={85,170,255}));
  connect(voltageSource.pin_n, singlePhaseLoad.pin_n) annotation (Line(points={{-40,-10},{-40,-20},{20,-20},{20,-10}}, color={85,170,255}));
  connect(powerSensor.voltageN, singlePhaseLoad.pin_n) annotation (Line(points={{-10,10},{-10,-20},{20,-20},{20,-10}}, color={85,170,255}));
  connect(powerSensor.voltageP, powerSensor.currentP) annotation (Line(points={{-10,30},{-20,30},{-20,20}}, color={85,170,255}));
  connect(voltageSource.pin_n, ground.pin) annotation (Line(points={{-40,-10},{-40,-30}}, color={85,170,255}));
  connect(ramp.y, singlePhaseLoad.power) annotation (Line(points={{49,0},{32,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(preserveAspectRatio=false)),
    experiment(
      __Dymola_NumberOfIntervals=1000,
      Tolerance=1e-06,
      __Dymola_Algorithm="Dassl"));
end SinglePhaseLoad;
