within VirtualHomeStorage.ComponentTesting;
model SimpleBattery "Simple battery with current source"
  extends Modelica.Icons.Example;
  Modelica.Electrical.Analog.Basic.Ground ground annotation (
    Placement(visible = true, transformation(origin={-30,-30},  extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Batteries.BatteryStacks.CellStack cellStack(cellData=cellData) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-30,10})));
  Modelica.Electrical.Analog.Sources.ConstantCurrent constantCurrent(I=25)  annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={28,10})));
  parameter Modelica.Electrical.Batteries.ParameterRecords.CellData cellData(
    OCVmax=14,
    OCVmin=10,
    Qnom=90000,
    Ri=14) annotation (Placement(transformation(extent={{60,60},{80,80}})));

equation
  connect(ground.p, cellStack.n) annotation (Line(points={{-30,-20},{-30,-3.55271e-15}}, color={0,0,255}));
  connect(cellStack.p, constantCurrent.p) annotation (Line(points={{-30,20},{-30,
          40},{28,40},{28,20}},                                                                        color={0,0,255}));
  connect(constantCurrent.n, ground.p) annotation (Line(points={{28,0},{28,-20},{-30,-20}}, color={0,0,255}));
  annotation (
    defaultComponentName = "moduleData",
    defaultComponentPrefixes = "parameter",
    Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2, 2})),
    Diagram(coordinateSystem(initialScale = 0.1)),
    experiment(
      StopTime=3600,
      Interval=0.1,
      Tolerance=1e-06),
    __OpenModelica_simulationFlags(jacobian = "coloredNumerical", nls = "newton", s = "dassl", lv = "LOG_STATS"));
end SimpleBattery;
