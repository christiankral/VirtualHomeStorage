within VirtualHomeStorage.ComponentTesting;
model AnalogLoad
  extends Modelica.Icons.Example;
  Loads.AnalogLoad load annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={20,0})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(transformation(extent={{-50,-50},{-30,-30}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage voltageSource(
    V=100) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-40,0})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor annotation (Placement(transformation(extent={{-20,10},{0,30}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=500,
    duration=0.5,
    offset=500,
    startTime=0.25) annotation (Placement(transformation(extent={{70,-10},{50,10}})));
equation
  connect(ramp.y, load.power) annotation (Line(points={{49,0},{32,0}}, color={0,0,127}));
  connect(voltageSource.p, powerSensor.pc) annotation (Line(points={{-40,10},{-40,20},{-20,20}}, color={0,0,255}));
  connect(powerSensor.pc, powerSensor.pv) annotation (Line(points={{-20,20},{-20,30},{-10,30}}, color={0,0,255}));
  connect(powerSensor.nc, load.p) annotation (Line(points={{0,20},{20,20},{20,10}}, color={0,0,255}));
  connect(load.n, ground.p) annotation (Line(points={{20,-10},{20,-20},{-40,-20},{-40,-30}}, color={0,0,255}));
  connect(ground.p, voltageSource.n) annotation (Line(points={{-40,-30},{-40,-10}}, color={0,0,255}));
  connect(powerSensor.nv, ground.p) annotation (Line(points={{-10,10},{-10,-20},{-40,-20},{-40,-30}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(preserveAspectRatio=false)),
    experiment(
      __Dymola_NumberOfIntervals=1000,
      Tolerance=1e-06,
      __Dymola_Algorithm="Dassl"));
end AnalogLoad;
