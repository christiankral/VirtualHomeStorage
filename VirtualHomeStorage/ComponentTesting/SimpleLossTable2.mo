within VirtualHomeStorage.ComponentTesting;
model SimpleLossTable2 "Test of simple DC loss model"
  extends Modelica.Icons.Example;
  VirtualHomeStorage.Converter.Components.Loss loss
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation (Placement(transformation(extent={{-100,-50},{-80,-30}})));
  Modelica.Electrical.Analog.Basic.Ground ground2 annotation (Placement(transformation(extent={{50,-52},{70,-32}})));
  Modelica.Electrical.Analog.Sources.SignalCurrent signalCurrent annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={60,0})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=200) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-90,0})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor1 annotation (Placement(transformation(extent={{-50,10},{-30,30}})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor2 annotation (Placement(transformation(extent={{30,10},{50,30}})));
  Modelica.Blocks.Tables.CombiTable2Ds combiTable(table=[0,100,200; 0,10,20;
        2000,100,400], smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments)
    annotation (Placement(transformation(extent={{-8,-60},{12,-40}})));
  Modelica.Blocks.Sources.Ramp ramp(duration=1, height=10) annotation (Placement(transformation(extent={{100,-10},{80,10}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={-60,0})));
equation
  connect(ground1.p, constantVoltage.n) annotation (Line(points={{-90,-30},{-90,-10}}, color={0,0,255}));
  connect(loss.n1, constantVoltage.n) annotation (Line(points={{-10,-10},{-20,-10},{-20,-20},{-90,-20},{-90,-10}},
                                          color={0,0,255}));
  connect(loss.n2, signalCurrent.n) annotation (Line(points={{10,-10},{20,-10},{
          20,-20},{60,-20},{60,-10}}, color={0,0,255}));
  connect(ground2.p, signalCurrent.n) annotation (Line(points={{60,-32},{60,-10}}, color={0,0,255}));
  connect(loss.p2, powerSensor2.pc) annotation (Line(points={{10,10},{20,10},{20,
          20},{30,20}}, color={0,0,255}));
  connect(powerSensor2.pc, powerSensor2.pv) annotation (Line(points={{30,20},{30,30},{40,30}}, color={0,0,255}));
  connect(powerSensor2.nc, signalCurrent.p) annotation (Line(points={{50,20},{60,20},{60,10}}, color={0,0,255}));
  connect(powerSensor2.nv, ground2.p) annotation (Line(points={{40,10},{40,-20},{60,-20},{60,-32}}, color={0,0,255}));
  connect(powerSensor1.nv, ground1.p) annotation (Line(points={{-40,10},{-40,-20},{-90,-20},{-90,-30}}, color={0,0,255}));
  connect(powerSensor1.nc, loss.p1) annotation (Line(points={{-30,20},{-20,20},{
          -20,10},{-10,10}}, color={0,0,255}));
  connect(constantVoltage.p, powerSensor1.pc) annotation (Line(points={{-90,10},{-90,20},{-50,20}}, color={0,0,255}));
  connect(powerSensor1.pc, powerSensor1.pv) annotation (Line(points={{-50,20},{-50,30},{-40,30}}, color={0,0,255}));
  connect(ramp.y, signalCurrent.i) annotation (Line(points={{79,0},{72,0}}, color={0,0,127}));
  connect(voltageSensor.p, powerSensor1.pc) annotation (Line(points={{-60,10},{-60,20},{-50,20}}, color={0,0,255}));
  connect(voltageSensor.n, constantVoltage.n) annotation (Line(points={{-60,-10},{-60,-20},{-90,-20},{-90,-10}}, color={0,0,255}));
  connect(combiTable.y, loss.lossPower) annotation (Line(points={{13,-50},{20,-50},{20,-24},{6,-24},{6,-12}}, color={0,0,127}));
  connect(voltageSensor.v, combiTable.u2) annotation (Line(points={{-49,0},{-44,0},{-44,-56},{-10,-56}}, color={0,0,127}));
  connect(loss.outputPower, combiTable.u1) annotation (Line(points={{-6,-11},{-6,-30},{-20,-30},{-20,-44},{-10,-44}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(preserveAspectRatio=false)));
end SimpleLossTable2;
