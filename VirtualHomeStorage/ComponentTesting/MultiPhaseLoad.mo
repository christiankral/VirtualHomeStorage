within VirtualHomeStorage.ComponentTesting;
model MultiPhaseLoad
  extends Modelica.Icons.Example;
  Loads.MultiPhaseLoad multiPhaseLoad annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={20,0})));
  Modelica.Electrical.QuasiStatic.Polyphase.Sources.VoltageSource voltageSource(
    gamma(fixed=true, start=0),
    f=50,
    V=fill(230, 3)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-40,0})));
  Modelica.Electrical.QuasiStatic.Polyphase.Sensors.PowerSensor powerSensor
    annotation (Placement(transformation(extent={{-20,10},{0,30}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=500,
    duration=0.5,
    offset=500,
    startTime=0.25) annotation (Placement(transformation(extent={{70,-10},{50,10}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star star annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-40,-30})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground ground
    annotation (Placement(transformation(extent={{-50,-70},{-30,-50}})));
equation
  connect(ramp.y, multiPhaseLoad.power) annotation (Line(points={{49,0},{38,0},{38,-2.22045e-15},{32,-2.22045e-15}}, color={0,0,127}));
  connect(star.pin_n, ground.pin) annotation (Line(points={{-40,-40},{-40,-50}}, color={85,170,255}));
  connect(powerSensor.currentN, multiPhaseLoad.plug_p) annotation (Line(points={{0,20},{20,20},{20,10}}, color={85,170,255}));
  connect(multiPhaseLoad.plug_n, star.plug_p) annotation (Line(points={{20,-10},{20,-20},{-40,-20}}, color={85,170,255}));
  connect(star.plug_p, voltageSource.plug_n) annotation (Line(points={{-40,-20},{-40,-10}}, color={85,170,255}));
  connect(voltageSource.plug_p, powerSensor.currentP) annotation (Line(points={{-40,10},{-40,20},{-20,20}}, color={85,170,255}));
  connect(powerSensor.currentP, powerSensor.voltageP) annotation (Line(points={{-20,20},{-20,30},{-10,30}}, color={85,170,255}));
  connect(powerSensor.voltageN, star.plug_p) annotation (Line(points={{-10,10},{-10,-20},{-40,-20}}, color={85,170,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(preserveAspectRatio=false)),
    experiment(
      __Dymola_NumberOfIntervals=1000,
      Tolerance=1e-06,
      __Dymola_Algorithm="Dassl"));
end MultiPhaseLoad;
