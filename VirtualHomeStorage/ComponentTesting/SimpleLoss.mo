within VirtualHomeStorage.ComponentTesting;
model SimpleLoss "Test of simple DC loss model"
  extends Modelica.Icons.Example;
  VirtualHomeStorage.Converter.Components.Loss loss
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));
  Modelica.Electrical.Analog.Basic.Ground ground2 annotation (Placement(transformation(extent={{60,-52},{80,-32}})));
  Modelica.Electrical.Analog.Basic.Resistor resistor2(R=10) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={70,2})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=100) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-70,0})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor1 annotation (Placement(transformation(extent={{-50,10},{-30,30}})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor2 annotation (Placement(transformation(extent={{30,10},{50,30}})));
  Modelica.Blocks.Math.Gain gain(k=0.1) annotation (Placement(transformation(extent={{-8,-60},{12,-40}})));
equation
  connect(ground1.p, constantVoltage.n) annotation (Line(points={{-70,-30},{-70,-10}}, color={0,0,255}));
  connect(loss.n1, constantVoltage.n) annotation (Line(points={{-10,-10},{-20,-10},
          {-20,-20},{-70,-20},{-70,-10}}, color={0,0,255}));
  connect(loss.n2, resistor2.n) annotation (Line(points={{10,-10},{20,-10},{20,
          -20},{70,-20},{70,-8}}, color={0,0,255}));
  connect(ground2.p, resistor2.n) annotation (Line(points={{70,-32},{70,-8}}, color={0,0,255}));
  connect(loss.p2, powerSensor2.pc) annotation (Line(points={{10,10},{20,10},{
          20,20},{30,20}}, color={0,0,255}));
  connect(powerSensor2.pc, powerSensor2.pv) annotation (Line(points={{30,20},{30,30},{40,30}}, color={0,0,255}));
  connect(powerSensor2.nc, resistor2.p) annotation (Line(points={{50,20},{70,20},{70,12}}, color={0,0,255}));
  connect(powerSensor2.nv, ground2.p) annotation (Line(points={{40,10},{40,-20},{70,-20},{70,-32}}, color={0,0,255}));
  connect(powerSensor1.nv, ground1.p) annotation (Line(points={{-40,10},{-40,-20},{-70,-20},{-70,-30}}, color={0,0,255}));
  connect(powerSensor1.nc, loss.p1) annotation (Line(points={{-30,20},{-20,20},
          {-20,10},{-10,10}}, color={0,0,255}));
  connect(constantVoltage.p, powerSensor1.pc) annotation (Line(points={{-70,10},{-70,20},{-50,20}}, color={0,0,255}));
  connect(powerSensor1.pc, powerSensor1.pv) annotation (Line(points={{-50,20},{-50,30},{-40,30}}, color={0,0,255}));
  connect(loss.outputPower, gain.u) annotation (Line(points={{-6,-11},{-6,-30},
          {-20,-30},{-20,-50},{-10,-50}}, color={0,0,127}));
  connect(gain.y, loss.lossPower) annotation (Line(points={{13,-50},{20,-50},{20,-28},{6,-28},{6,-12}},
                                    color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(preserveAspectRatio=false)));
end SimpleLoss;
