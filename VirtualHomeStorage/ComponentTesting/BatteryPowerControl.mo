within VirtualHomeStorage.ComponentTesting;
model BatteryPowerControl "Power controlled battery"
  extends Modelica.Icons.Example;
  parameter Integer nsModule = 6 "Number of series connected modules";
  parameter Integer npModule = 1 "Number of parallel connected modules";
  // Weather data file name has to be passed without loadResource to ReaderTMY3 class of Buildings library
  parameter String fileName = Modelica.Utilities.Files.loadResource("modelica://VirtualHomeStorage/Resources/Irradiance/irradiance.txt") "File name";
  parameter String csvFileName = "energy.csv";
  parameter Modelica.Units.SI.Voltage VRef=cellData.OCVmax*cellStack.Ns
    "Reference voltage";
  Modelica.Electrical.Analog.Basic.Ground ground annotation (
    Placement(visible = true, transformation(origin={-50,-10},    extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Converter.QuasiStaticSinglePhaseLossConverterControlled converter(
    VRef=VRef,
    PRef=1000,
    Tc=1E-3) annotation (Placement(transformation(extent={{10,10},{30,30}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground groundAC
    annotation (Placement(transformation(extent={{80,-20},{100,0}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Sources.VoltageSource
    voltageSource(
    f=50,
    V=230,
    phi=0,
    gamma(fixed=true, start=0)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={90,20})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensorDC annotation (Placement(transformation(extent={{-10,30},{-30,50}})));
  Modelica.Blocks.Continuous.Integrator integrator(y(unit = "J")) annotation (
    Placement(transformation(extent={{-70,-30},{-90,-10}})));
  Modelica.Electrical.Batteries.BatteryStacks.CellStack cellStack(
    Ns=9,
    Np=2,
    cellData=cellData,
    SOC(start=0.5)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-50,20})));
  Modelica.Blocks.Sources.Constant const(k=-100) annotation (Placement(transformation(extent={{-90,-70},{-70,-50}})));
  parameter Modelica.Electrical.Batteries.ParameterRecords.CellData cellData(
    OCV_SOC=[0.0,0.7704615384615384; 0.05,0.8123076923076923; 0.1,0.832; 0.15,0.8467692307692307; 0.2,0.8590769230769232; 0.25,0.8664615384615385; 0.3,0.8726153846153846; 0.35,0.8775384615384615; 0.4,0.8836923076923077; 0.45,0.889846153846154; 0.5,0.8955076923076923; 0.55,0.9016615384615384; 0.6,0.9078153846153847; 0.65,0.9139692307692308; 0.7,0.9201230769230769; 0.75,0.926276923076923; 0.8,0.9324307692307692; 0.85,0.9385846153846155; 0.9,0.9476923076923077; 0.95,0.96; 1.0,1.0],
    OCVmax=4.0625,
    OCVmin=3.13,
    Qnom=90000,
    Ri=0.023,
    SOCmax=1,
    SOCmin=0,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    useLinearSOCDependency=false)                                                                                                                                                                                                         annotation (
    Placement(transformation(extent={{-60,60},{-40,80}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Sensors.PowerSensor powerSensorAC
    annotation (Placement(transformation(extent={{70,30},{50,50}})));
equation
  connect(converter.ac_n, groundAC.pin) annotation (
    Line(points={{30,14},{40,14},{40,0},{90,0}}, color = {85, 170, 255}));
  connect(groundAC.pin, voltageSource.pin_n) annotation (
    Line(points={{90,0},{90,10}},         color = {85, 170, 255}));
  connect(powerSensorDC.pc, powerSensorDC.pv) annotation (Line(points={{-10,40},{-10,50},{-20,50}}, color={0,0,255}));
  connect(powerSensorDC.nv, ground.p) annotation (Line(points={{-20,30},{-20,0},{-50,0}}, color={0,0,255}));
  connect(ground.p, converter.dc_n) annotation (
    Line(points={{-50,0},{0,0},{0,14},{10,14}},       color = {0, 0, 255}));
  connect(integrator.u, powerSensorDC.power) annotation (Line(points={{-68,-20},{-10,-20},{-10,29}}, color={0,0,127}));
  connect(cellStack.n, ground.p) annotation (Line(points={{-50,10},{-50,0}}, color={0,0,255}));
  connect(const.y, converter.powerRef) annotation (Line(points={{-69,-60},{26,-60},{26,8}}, color={0,0,127}));
  connect(converter.dc_p, powerSensorDC.pc) annotation (Line(points={{10,26},{0,26},{0,40},{-10,40}}, color={0,0,255}));
  connect(powerSensorDC.nc, cellStack.p) annotation (Line(points={{-30,40},{-50,40},{-50,30}}, color={0,0,255}));
  connect(powerSensorAC.currentP, voltageSource.pin_p) annotation (Line(points={{70,40},{90,40},{90,30}}, color={85,170,255}));
  connect(powerSensorAC.currentN, converter.ac_p) annotation (Line(points={{50,40},
          {40,40},{40,26},{30,26}},                                                                          color={85,170,255}));
  connect(powerSensorAC.voltageN, groundAC.pin) annotation (Line(points={{60,30},{60,0},{90,0}}, color={85,170,255}));
  connect(powerSensorAC.voltageP, powerSensorAC.currentP) annotation (Line(points={{60,50},{70,50},{70,40}}, color={85,170,255}));
  annotation (
    experiment(
      StopTime=500,
      Interval=0.1,
      Tolerance=1e-07,
      __Dymola_Algorithm="Dassl"),
    Documentation(revisions = "<html>
</html>",
        info="<html>
<p>This example is based on weather data taken from 
<a href=\"https://energyplus.net/weather-location/europe_wmo_region_6/AUT//AUT_Vienna.Schwechat.110360_IWEC\">https://energyplus.net</a>. 
The EPW file was converted using a Java script provided by the 
<a href=\"https://github.com/lbl-srg/modelica-buildings\">Buildings</a> library.</p>
<p><code>
java -jar .../Buildings/Resources/bin/ConvertWeatherData.jar .../PhotoVoltaics/Resources/WeatherData/AUT_Vienna.Schwechat.110360_IWEC.epw
</code></p>
<p>The weather data are distributed under the 
<a href=\"https://energyplus.net/licensing\">EnergyPlus Licensing</a> conditions, see 
<a href=\"PhotoVoltaics.UsersGuide.License\">License</a>.
</p>
</html>"));
end BatteryPowerControl;
