within VirtualHomeStorage.Loads;
model SinglePhaseLoad
  extends Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.TwoPin;
  import Modelica.ComplexMath.real;
  import Modelica.ComplexMath.conj;
  parameter Modelica.Units.SI.Voltage VRef=230 "Reference line to line voltage";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";

  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.VariableConductor conductor
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Sensors.PowerSensor powerSensor
    annotation (Placement(transformation(extent={{40,10},{60,-10}})));
  Modelica.Blocks.Interfaces.RealInput power(unit="W") "Power" annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={0,120})));
  Modelica.Blocks.Math.Feedback feedback annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={0,70})));
  Modelica.ComplexBlocks.ComplexMath.ComplexToReal complexToReal annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={40,40})));
  Modelica.Blocks.Continuous.Integrator    integrator(   k=1/Ti/VRef^2,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0)                                                            annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,38})));
equation
  connect(pin_p, conductor.pin_p) annotation (Line(points={{-100,0},{-10,0}}, color={85,170,255}));
  connect(conductor.pin_n, powerSensor.currentP) annotation (Line(points={{10,0},{40,0}}, color={85,170,255}));
  connect(powerSensor.currentN, pin_n) annotation (Line(points={{60,0},{100,0}}, color={85,170,255}));
  connect(powerSensor.voltageP, pin_p) annotation (Line(points={{50,-10},{50,-20},{-100,-20},{-100,0}}, color={85,170,255}));
  connect(powerSensor.voltageN, pin_n) annotation (Line(points={{50,10},{50,20},{100,20},{100,0}}, color={85,170,255}));
  connect(feedback.u1, power) annotation (Line(points={{0,78},{0,78},{0,120}}, color={0,0,127}));
  connect(complexToReal.u, powerSensor.apparentPower)
    annotation (Line(points={{40,28},{40,11}}, color={85,170,255}));
  connect(complexToReal.re, feedback.u2) annotation (Line(points={{34,52},{34,70},{8,70},{8,70}}, color={0,0,127}));
  connect(integrator.u, feedback.y) annotation (Line(points={{2.22045e-15,50},{2.22045e-15,56},{0,56},{0,61}}, color={0,0,127}));
  connect(integrator.y, conductor.G_ref) annotation (Line(points={{-2.22045e-15,27},{-2.22045e-15,20},{0,20},{0,12}}, color={0,0,127}));
  annotation (defaultComponentName="load",
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={Rectangle(extent={{-100,40},{100,-40}},   lineColor={85,170,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),                                                                                                    Text(
          extent={{-40,40},{40,-40}},
          lineColor={85,170,255},
          textString="P"),
      Text(lineColor = {0,0,255}, extent={{-150,50},{150,90}},     textString = "%name")}),
                                                                 Diagram(coordinateSystem(preserveAspectRatio=false)));
end SinglePhaseLoad;
