within VirtualHomeStorage.Loads;
model AnalogLoad
  extends Modelica.Electrical.Analog.Interfaces.TwoPin;
  parameter Modelica.Units.SI.Voltage VRef=230 "Reference line to line voltage";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";

  Modelica.Electrical.Analog.Basic.VariableConductor conductor annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Electrical.Analog.Sensors.PowerSensor  powerSensor annotation (Placement(transformation(extent={{40,10},{60,-10}})));
  Modelica.Blocks.Interfaces.RealInput power(unit="W") "Power" annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={0,120})));
  Modelica.Blocks.Math.Feedback feedback annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={0,70})));
  Modelica.Blocks.Continuous.Integrator    integrator(
    k=1/Ti/VRef^2,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,38})));
equation
  connect(feedback.u1, power) annotation (Line(points={{0,78},{0,78},{0,120}}, color={0,0,127}));
  connect(integrator.u, feedback.y) annotation (Line(points={{2.22045e-15,50},{2.22045e-15,56},{0,56},{0,61}}, color={0,0,127}));
  connect(p, conductor.p) annotation (Line(points={{-100,0},{-10,0}}, color={0,0,255}));
  connect(conductor.n, powerSensor.pc) annotation (Line(points={{10,0},{40,0}}, color={0,0,255}));
  connect(powerSensor.nc, n) annotation (Line(points={{60,0},{100,0}}, color={0,0,255}));
  connect(powerSensor.pv, p) annotation (Line(points={{50,-10},{50,-20},{-100,-20},{-100,0}}, color={0,0,255}));
  connect(powerSensor.nv, n) annotation (Line(points={{50,10},{50,20},{100,20},{100,0}}, color={0,0,255}));
  connect(powerSensor.power, feedback.u2) annotation (Line(points={{40,11},{40,70},{8,70}}, color={0,0,127}));
  connect(integrator.y, conductor.G) annotation (Line(points={{0,27},{0,12}}, color={0,0,127}));
  annotation (defaultComponentName="load",
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={Rectangle(extent={{-100,40},{100,-40}},   lineColor={0,0,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-40,40},{40,-40}},
          lineColor={0,0,255},
          textString="P"),
      Text(lineColor = {0,0,255}, extent={{-150,50},{150,90}}, textString = "%name")}), Diagram(coordinateSystem(preserveAspectRatio=false)));
end AnalogLoad;
