within VirtualHomeStorage.Loads;
model MultiPhaseLoad
  extends Modelica.Electrical.QuasiStatic.Polyphase.Interfaces.TwoPlug;
  import Modelica.ComplexMath.real;
  import Modelica.ComplexMath.conj;
  parameter Modelica.Units.SI.Voltage VRef=230 "Reference line to line voltage";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";

  Modelica.Blocks.Interfaces.RealInput power(unit="W") "Power" annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={0,120})));
  Modelica.Blocks.Routing.Replicator replicator(nout=3) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,48})));
  Modelica.Blocks.Math.Gain gain(final k=1/m) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,80})));
  SinglePhaseLoad singlePhaseLoad[m](VRef=fill(VRef, m), Ti=fill(Ti, m)) annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
equation
  connect(gain.u, power) annotation (Line(points={{0,92},{0,120}}, color={0,0,127}));
  connect(gain.y, replicator.u) annotation (Line(points={{0,69},{2.22045e-15,69},{2.22045e-15,60}}, color={0,0,127}));
  connect(plugToPins_p.pin_p, singlePhaseLoad.pin_p) annotation (Line(points={{-68,0},{-10,0}}, color={85,170,255}));
  connect(plugToPins_n.pin_n, singlePhaseLoad.pin_n) annotation (Line(points={{68,0},{10,0}}, color={85,170,255}));
  connect(singlePhaseLoad.power, replicator.y) annotation (Line(points={{0,12},{0,37}}, color={0,0,127}));
  annotation (defaultComponentName="load",
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={Rectangle(extent={{-100,40},{100,-40}},   lineColor={85,170,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),                                                                                                    Text(
          extent={{-40,40},{40,-40}},
          lineColor={85,170,255},
          textString="P"),
      Text(lineColor = {0,0,255}, extent={{-150,50},{150,90}},     textString = "%name")}),
                                                                 Diagram(coordinateSystem(preserveAspectRatio=false)));
end MultiPhaseLoad;
