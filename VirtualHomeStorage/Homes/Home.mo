within VirtualHomeStorage.Homes;
model Home "Home model with PV plant and load"
  parameter Integer nsModule = 11 "Number of series connected PV modules";
  parameter Integer npModule = 1 "Number of parallel connected PV modules";
  parameter Modelica.Units.SI.Voltage VRef=400/sqrt(3)
    "Reference voltage of PV converter";
  parameter Modelica.Units.SI.Time Ti=1E-6
    "Internal integration time constant of PV converter";
  parameter Modelica.Units.SI.Power lossTable[:,:]=[0,47; 7000,209]
    "PV converter output power vs loss table [...; output_power, loss_power; ...]";
  parameter Real gainLoad = 1 "Input power times gainLoad equals actual load";
  parameter VirtualHomeStorage.Records.AEG_AS_M605 moduleData annotation (
    Placement(visible = true, transformation(origin={-60,82},     extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  VirtualHomeStorage.Loads.SinglePhaseLoad load annotation (
    Placement(visible = true, transformation(origin={52,0},     extent = {{-10, 10}, {10, -10}}, rotation = 270)));
  VirtualHomeStorage.Sensors.EnergySensorsSinglePhase energyMeterTotal annotation (
    Placement(visible = true, transformation(origin={80,20},    extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground groundPV annotation (
    Placement(visible = true, transformation(origin={-60,-30},  extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PhotoVoltaics.Components.SimplePhotoVoltaics.SimplePlantSymmetric plantPV(moduleData = moduleData, T = 298.15, useConstantIrradiance = false, nsModule = nsModule, npModule = npModule) annotation (
    Placement(visible = true, transformation(origin={-60,0},     extent = {{-10, 10}, {10, -10}}, rotation = -90)));
  Converter.QuasiStaticSinglePhaseLossConverter                       converterPV(table=
        lossTable)                                                                annotation (
    Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensorPV annotation (
    Placement(transformation(extent={{-40,12},{-20,32}})));
  Modelica.Blocks.Interfaces.RealInput variableIrradiance
    "Variable irradiance input"
    annotation (Placement(transformation(extent={{-140,40},{-100,80}})));
  Modelica.Blocks.Interfaces.RealInput loadPower "Variable irradiance input"
    annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));
  PhotoVoltaics.Components.Blocks.MPTrackerSample mpTracker(
    samplePeriod=60,
    VmpRef=nsModule*moduleData.VmpRef,
    ImpRef=moduleData.ImpRef) annotation (
    Placement(transformation(extent={{-30,-50},{-10,-30}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.PositivePin pin_p
    "Positive pin of voltage path"
    annotation (Placement(transformation(extent={{90,50},{110,70}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.NegativePin pin_n
    "Negative pin of voltage path"
    annotation (Placement(transformation(extent={{90,-70},{110,-50}})));
  Modelica.Blocks.Interfaces.RealOutput energyPos
    "Positive energy determined from positive power" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-100,-110})));
  Modelica.Blocks.Interfaces.RealOutput powerPos "Positive power" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-60,-110})));
  Modelica.Blocks.Interfaces.RealOutput powerNeg "Negative power" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={60,-110})));
  Modelica.Blocks.Interfaces.RealOutput energyNeg
    "Negative energy determined from negative power" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={100,-110})));
  Modelica.Blocks.Interfaces.RealOutput power
    "Positive and negative (total) power" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-110})));
  Modelica.Blocks.Math.Gain gain(k=gainLoad)
    annotation (Placement(transformation(extent={{-80,-70},{-60,-50}})));
equation
  connect(energyMeterTotal.pv, energyMeterTotal.pc) annotation (
    Line(points={{80,30},{90,30},{90,20}},        color = {85, 170, 255}));
  connect(energyMeterTotal.nc, load.pin_p) annotation (
    Line(points={{70,20},{52,20},{52,10}},        color = {85, 170, 255}));
  connect(groundPV.p, plantPV.n) annotation (
    Line(points={{-60,-20},{-60,-10}},    color = {0, 0, 255}));
  connect(groundPV.p, converterPV.dc_n) annotation (
    Line(points={{-60,-20},{-20,-20},{-20,-6},{-10,-6}},        color = {0, 0, 255}));
  connect(plantPV.p, powerSensorPV.pc) annotation (
    Line(points={{-60,10},{-60,22},{-40,22}},        color = {0, 0, 255}));
  connect(powerSensorPV.nc, converterPV.dc_p) annotation (
    Line(points={{-20,22},{-20,6},{-10,6}},          color = {0, 0, 255}));
  connect(powerSensorPV.pc, powerSensorPV.pv) annotation (
    Line(points={{-40,22},{-40,32},{-30,32}},        color = {0, 0, 255}));
  connect(powerSensorPV.nv, groundPV.p) annotation (
    Line(points={{-30,12},{-30,-20},{-60,-20}},      color = {0, 0, 255}));
  connect(converterPV.ac_p, energyMeterTotal.nc) annotation (
    Line(points={{10,6},{40,6},{40,20},{70,20}},           color = {85, 170, 255}));

  connect(powerSensorPV.power,mpTracker. power) annotation (
    Line(points={{-40,11},{-40,-40},{-32,-40}},        color = {0, 0, 127}));
  connect(mpTracker.vRef, converterPV.vDCRef) annotation (
    Line(points={{-9,-40},{6,-40},{6,-12}},            color = {0, 0, 127}));
  connect(plantPV.variableIrradiance, variableIrradiance) annotation (Line(
        points={{-72,0},{-90,0},{-90,60},{-120,60}}, color={0,0,127}));
  connect(energyMeterTotal.pv, pin_p)
    annotation (Line(points={{80,30},{80,60},{100,60}}, color={85,170,255}));
  connect(energyMeterTotal.nv, pin_n)
    annotation (Line(points={{80,10},{80,-60},{100,-60}},color={85,170,255}));
  connect(load.pin_n, pin_n) annotation (Line(points={{52,-10},{52,-20},{80,-20},
          {80,-60},{100,-60}},color={85,170,255}));
  connect(converterPV.ac_n, pin_n) annotation (Line(points={{10,-6},{40,-6},{40,
          -20},{80,-20},{80,-60},{100,-60}},color={85,170,255}));
  connect(energyMeterTotal.energyPos, energyPos) annotation (Line(points={{70,9},{
          70,-76},{-100,-76},{-100,-110}},  color={0,0,127}));
  connect(energyMeterTotal.powerPos, powerPos) annotation (Line(points={{74,9},{
          74,-80},{-60,-80},{-60,-110}}, color={0,0,127}));
  connect(energyMeterTotal.powerNeg, powerNeg) annotation (Line(points={{86,9},{
          86,-84},{60,-84},{60,-110}}, color={0,0,127}));
  connect(energyMeterTotal.energyNeg, energyNeg) annotation (Line(points={{90,9},{
          90,-84},{100,-84},{100,-110}},  color={0,0,127}));
  connect(energyMeterTotal.power, power) annotation (Line(points={{91,14},{94,14},
          {94,-40},{40,-40},{40,-72},{0,-72},{0,-110}}, color={0,0,127}));
  connect(loadPower, gain.u)
    annotation (Line(points={{-120,-60},{-82,-60}}, color={0,0,127}));
  connect(gain.y, load.power) annotation (Line(points={{-59,-60},{26,-60},{26,0},
          {40,0}}, color={0,0,127}));
  annotation (Diagram(graphics={                                                                                                                                                                                                        Rectangle(origin={
              -31.5151,-7.6923},                                                                                                                                                                                                        fillColor = {253, 213, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-48.4849, 57.6923}, {51.5151, -42.3077}}),
                                                                                                                                                                                                    Rectangle(origin={45,
              1.923},                                                                                                                                                                                                        fillColor = {209, 255, 183}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-15, 48.077}, {15, -51.923}}),
                        Rectangle(origin={80.625,6.7308},     fillColor = {255, 225, 207}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                            FillPattern.Solid, extent = {{-20.625, 43.2692}, {19.375, -56.7308}})}),
      Icon(graphics={
        Polygon(
          points={{-100,60},{0,100},{100,60},{-100,60}},
          lineColor={0,0,0},
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-100,60},{100,-100}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-80,78},{-24,100},{-20,92},{-76,70},{-80,78}},
          lineColor={0,0,0},
          fillColor={85,85,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-80,40},{-20,0}},
          lineColor={0,0,0},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{20,40},{80,0}},
          lineColor={0,0,0},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-80,-20},{-20,-60}},
          lineColor={0,0,0},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{20,-20},{80,-100}},
          lineColor={0,0,0},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid)}));
end Home;
