within VirtualHomeStorage.Controller;
model ChargeController "Controlling the battery charge"
  import Modelica.Constants.eps;
  parameter Modelica.Units.SI.Power powerMax "Maximum power";
  parameter Modelica.Units.SI.Power powerMin=5E-3
    "Minimum power, to be much smaller than maximum power";
  parameter Real SOCmin = 0.05 "Minimum SOC";
  parameter Real SOCmax = 0.95 "Maximum SOC";
  parameter Real SOCslope = 0.05 "Hyseresis band of SOC";
  parameter Real dSOC = 1E-4 "SOC tolerance to hardly switch state";
  parameter Modelica.Units.SI.Time T=1
    "Smoothing time constant of output power signal";
  Modelica.Blocks.Interfaces.RealInput SOC "State of charge"
                                           annotation (
    Placement(visible = true, transformation(origin = {-120, 100}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 100}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Units.SI.Power powerBatteryUnsmoothed "Unsmoothed output power";
  Modelica.Blocks.Interfaces.RealOutput powerBattery "Power command to battery"
                                                     annotation (
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 180)));
  Modelica.Blocks.Interfaces.RealInput powerRef "Reference power to be set"
                                                annotation (
    Placement(visible = true, transformation(origin = {-120, -100}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -100}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Units.SI.Power upperPowerLimit "Upper power limit";
  Modelica.Units.SI.Power lowerPowerLimit "Lower power limit";
  discrete Boolean overCharged(start = false) "Indicate if overcharged";
  discrete Boolean underCharged(start = false) "Indicate if undercharged";
algorithm
  when SOC > SOCmax - dSOC then
    overCharged := true;
  end when;
  when powerRef < (-powerMin) then
    overCharged := false;
  end when;
  when SOC < SOCmin + dSOC then
    underCharged := true;
  end when;
  when powerRef > powerMin then
    underCharged := false;
  end when;
equation
  upperPowerLimit = smooth(0,
    noEvent(if not overCharged then
      smooth(0, noEvent(if SOC < SOCmax - SOCslope then
                  powerMax
                elseif SOC < SOCmax then
                  powerMax - (SOC - (SOCmax - SOCslope)) / SOCslope * (powerMax + powerMin)
               else -powerMin))
    else
      smooth(0, noEvent(if SOC < SOCmax - 2 * SOCslope then
                  powerMax
                elseif SOC < SOCmax - SOCslope then
                  powerMax - (SOC - (SOCmax - 2 * SOCslope)) / SOCslope * (powerMax + powerMin)
                else -powerMin))));

  lowerPowerLimit = smooth(0,
    noEvent(if not underCharged then
      smooth(0, noEvent(if SOC < SOCmin then
                  powerMin
                elseif SOC < SOCmin + SOCslope then
                  powerMin - (powerMax + powerMin) * (SOC - SOCmin) / SOCslope
                else -powerMax))
    else smooth(0, noEvent(if SOC < SOCmin + SOCslope then
                     powerMin
                   elseif SOC < SOCmin + 2 * SOCslope then
                     powerMin - (powerMax + powerMin) * (SOC - (SOCmin + SOCslope)) / SOCslope
                   else -powerMax))));

  powerBatteryUnsmoothed = smooth(0, noEvent(if powerRef > upperPowerLimit then
                             upperPowerLimit
                           elseif powerRef < lowerPowerLimit then
                             lowerPowerLimit
                           else powerRef));
  der(powerBattery) = (powerBatteryUnsmoothed - powerBattery)/T;
  annotation (
    Icon(graphics={  Rectangle(extent = {{-100, 100}, {100, -100}}, lineColor={0,0,0},     fillColor={170,213,255},
            fillPattern =                                                                                                           FillPattern.Solid),   Line(origin = {-70, 0}, points = {{-30, 0}, {30, 0}, {30, 0}}), Line(origin = {70, 0}, points = {{30, 0}, {-30, 0}, {-30, 0}}), Rectangle(origin = {0, 0}, extent = {{-40, 8}, {40, -8}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),                                                                                                                                                                                                        Line(origin = {1, 0}, points = {{-41, -20}, {-21, -20}, {19, 20}, {41, 20}, {41, 20}}), Bitmap(extent = {{-42, 16}, {-42, 16}}), Text(origin = {-40, 70}, extent = {{-50, 20}, {50, -20}}, textString = "SOC"), Text(origin = {-40, -70}, extent = {{-50, 20}, {50, -20}}, textString = "power", lineColor = {0, 0, 0}),
                                               Text(
          extent={{-150,150},{150,110}},
          textString="%name",
          lineColor={0,0,255})},                                                                                                                                                                                                        coordinateSystem(initialScale = 0.1)),
    experiment(__Dymola_NumberOfIntervals = 50000, Tolerance = 1e-06),
    Documentation(info="<html>
<p>The charge controller sets the battery power output <code>powerBattery</code> depending on the two inputs <code>SOC</code> (state of charge) 
and the reference power to be set, <code>powerRef</code>.</p>

<p>The controller uses two internal discrete variables:</p>
<ul>
<li><code>underCharged</code> is true, if the battery <code>SOC</code> becomes less than the SOC limit <code>SOCmin</code></li>
<li><code>overCharged</code> is true, if the battery <code>SOC</code> becomes greater than the SOC limit <code>SOCmax</code></li>
</ul>

<p>Additionally the lower and upper power limit are set, following a hysteresis characteristic</p>

<ul>
<li><strong>red branch</strong> if not <code>overCharged</code></li>
<li><strong>green branch</strong> if <code>overCharged</code></li>
<li><strong>blue branch</strong> if not <code>underCharged</code></li>
<li><strong>purple branch</strong> if <code>underCharged</code></li>
</ul>

<p>
<img src=\"modelica://VirtualHomeStorage/Resources/Images/Controller/ChargeController/powerLimits.png\"
     alt=\"powerLimits.png\"> 
</p>
</html>"));
end ChargeController;
