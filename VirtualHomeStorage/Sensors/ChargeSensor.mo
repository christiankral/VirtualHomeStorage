within VirtualHomeStorage.Sensors;
model ChargeSensor "Charge sensors"
  parameter Modelica.Units.SI.ElectricCharge Qnom(displayUnit="A.h")
    "Nominal (maximum) charge";
  parameter Real SOCstart(start = 0.5) "Start value of SOC";
  extends Modelica.Icons.RoundSensor;
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (Placement(visible=true, transformation(
        origin={-30,0},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Blocks.Continuous.Integrator integrator(
    k=-1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=SOCstart*Qnom) annotation (Placement(visible=true, transformation(
        origin={0,-30},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput Q annotation (
    Placement(visible = true, transformation(origin={100,-110},  extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={100,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Math.Gain gain(k=1/Qnom) annotation (Placement(visible=true, transformation(
        origin={-2,-70},
        extent={{10,-10},{-10,10}},
        rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput SOC annotation (
    Placement(visible = true, transformation(origin={-100,-110},  extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin = {-100, -110}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pBattery annotation (
    Placement(visible = true, transformation(origin={-100,0},    extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin nGrid annotation (
    Placement(visible = true, transformation(origin={100,0},    extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(currentSensor.i, integrator.u) annotation (Line(points={{-30,-11},{-30,-30},{-12,-30}}, color={0,0,127}));
  connect(integrator.y, Q) annotation (Line(points={{11,-30},{100,-30},{100,-110}}, color={0,0,127}));
  connect(integrator.y, gain.u) annotation (Line(points={{11,-30},{29,-30},{29,-70},{10,-70}}, color={0,0,127}));
  connect(gain.y, SOC) annotation (Line(points={{-13,-70},{-100,-70},{-100,-110}}, color={0,0,127}));
  connect(currentSensor.n, nGrid) annotation (Line(points={{-20,0},{100,0}}, color={0,0,255}));
  connect(currentSensor.p, pBattery) annotation (Line(points={{-40,0},{-100,0}}, color={0,0,255}));
  annotation (
    Icon(graphics={  Text(origin = {-1, -32}, extent = {{-47, 16}, {47, -16}}, textString = "As"),
                     Text(origin={-70,-80},  extent={{-30,20},{30,-20}},
          textString="1",
          lineColor={0,0,0}),
        Line(
          points={{-100,-100},{-100,-80},{-58,-38}},
          color={0,0,0},
          pattern=LinePattern.Dash),
                     Text(origin={62,-80},   extent={{-30,20},{30,-20}},      textString = "As"),
        Line(
          points={{100,-100},{100,-80},{58,-38}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,0},{-90,0}}, color={0,0,255})}));
end ChargeSensor;
