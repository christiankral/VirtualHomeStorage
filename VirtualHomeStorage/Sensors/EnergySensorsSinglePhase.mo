within VirtualHomeStorage.Sensors;
model EnergySensorsSinglePhase "Single phase sensor to determine positive, negative and total power and energy"
  extends Modelica.Icons.RoundSensor;
  Modelica.Electrical.QuasiStatic.SinglePhase.Sensors.PowerSensor powerSensor
    annotation (Placement(visible=true, transformation(
        origin={0,0},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.ComplexBlocks.ComplexMath.ComplexToReal complexToReal annotation (Placement(visible=true, transformation(
        origin={-14,-30},
        extent={{-10,10},{10,-10}},
        rotation=-90)));
  Modelica.Blocks.Nonlinear.Limiter limiterNeg(final uMax=0, final uMin=-
        Modelica.Constants.inf) annotation (Placement(visible=true,
        transformation(
        origin={-40,-50},
        extent={{10,-10},{-10,10}},
        rotation=0)));
  Modelica.Blocks.Nonlinear.Limiter limiterPos(final uMax=Modelica.Constants.inf,
      final uMin=0) annotation (Placement(visible=true, transformation(
        origin={40,-50},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput energyNeg "Negative energy determined from negative power"
                                                  annotation (
    Placement(visible = true, transformation(origin={-100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={-100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput powerNeg "Negative power"
                                                 annotation (
    Placement(visible = true, transformation(origin = {-60, -110}, extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin = {-60, -110}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput energyPos "Positive energy determined from positive power"
                                                  annotation (
    Placement(visible = true, transformation(origin={100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput powerPos "Positive power"
                                                 annotation (
    Placement(visible = true, transformation(origin={60,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={60,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Continuous.Integrator integratorPos(final k=1)
                                                      annotation (
    Placement(visible = true, transformation(origin = {80, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.Integrator integratorNeg(final k=1) annotation (Placement(visible=true, transformation(
        origin={-80,-50},
        extent={{10,-10},{-10,10}},
        rotation=0)));
  Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.PositivePin pc
    "Positive pin of current path" annotation (Placement(
      visible=true,
      transformation(
        origin={-100,0},
        extent={{-10,-10},{10,10}},
        rotation=0),
      iconTransformation(
        origin={-100,0},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.NegativePin nv
    "Negative pin of voltage path" annotation (Placement(
      visible=true,
      transformation(
        origin={0,-100},
        extent={{-10,-10},{10,10}},
        rotation=0),
      iconTransformation(
        origin={0,-100},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.NegativePin nc
    "Negative pin of current path" annotation (Placement(
      visible=true,
      transformation(
        origin={100,0},
        extent={{-10,-10},{10,10}},
        rotation=0),
      iconTransformation(
        origin={100,0},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Electrical.QuasiStatic.SinglePhase.Interfaces.PositivePin pv
    "Positive pin of voltage path" annotation (Placement(
      visible=true,
      transformation(
        origin={0,100},
        extent={{-10,-10},{10,10}},
        rotation=0),
      iconTransformation(
        origin={0,100},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput power "Positive and negative (total) power" annotation (Placement(
      visible=true,
      transformation(
        origin={-20,-110},
        extent={{-10,-10},{10,10}},
        rotation=-90),
      iconTransformation(
        origin={-110,-60},
        extent={{-10,-10},{10,10}},
        rotation=180)));
equation
  connect(powerSensor.apparentPower, complexToReal.u) annotation (Line(points={
          {-10,-11},{-10,-14},{-14,-14},{-14,-18}}, color={85,170,255}));
  connect(complexToReal.re, limiterNeg.u) annotation (Line(points={{-20,-42},{-20,-50},{-28,-50}}, color={0,0,127}));
  connect(limiterPos.u, complexToReal.re) annotation (Line(points={{28,-50},{-20,-50},{-20,-42}}, color={0,0,127}));
  connect(limiterNeg.y, powerNeg) annotation (
    Line(points={{-51,-50},{-60,-50},{-60,-110}},        color = {0, 0, 127}));
  connect(limiterPos.y, powerPos) annotation (
    Line(points={{51,-50},{60,-50},{60,-110}},                                          color = {0, 0, 127}));
  connect(limiterPos.y, integratorPos.u) annotation (
    Line(points={{51,-50},{68,-50},{68,-50},{68,-50}},          color = {0, 0, 127}));
  connect(integratorPos.y, energyPos) annotation (
    Line(points={{91,-50},{100,-50},{100,-80},{90,-80},{90,-110},{100,-110}},             color = {0, 0, 127}));
  connect(limiterNeg.y, integratorNeg.u) annotation (Line(points={{-51,-50},{-66,-50},{-66,-50},{-68,-50}}, color={0,0,127}));
  connect(integratorNeg.y, energyNeg) annotation (Line(points={{-91,-50},{-100,-50},{-100,-110}}, color={0,0,127}));
  connect(powerSensor.currentP, pc) annotation (Line(points={{-10,0},{-102,0},{-102,0},{-100,0}}, color={85,170,255}));
  connect(powerSensor.voltageN, nv) annotation (Line(points={{0,-10},{0,-10},{0,-100},{0,-100}}, color={85,170,255}));
  connect(powerSensor.currentN, nc) annotation (Line(points={{10,0},{100,0},{100,0},{100,0}}, color={85,170,255}));
  connect(powerSensor.voltageP, pv) annotation (Line(points={{0,10},{0,10},{0,100},{0,100}}, color={85,170,255}));
  connect(complexToReal.re, power) annotation (Line(points={{-20,-42},{-20,-110}}, color={0,0,127}));
  annotation (Icon(graphics={
        Line(points={{-40,-40},{-20,-40}}, color={0,0,0}),
        Line(points={{18,-40},{38,-40}}, color={0,0,0}),
        Line(points={{28,-30},{28,-50}}, color={0,0,0}),
        Line(
          points={{-100,-60},{-80,-60},{-58,-38}},
          color={0,0,0},
          pattern=LinePattern.Dash),           Text(
          extent={{-150,150},{150,110}},
          textString="%name",
          lineColor={0,0,255}),
        Text(
          extent={{-130,-10},{-70,-50}},
          lineColor={0,0,255},
          pattern=LinePattern.Dash,
          fillColor={224,237,255},
          fillPattern=FillPattern.Solid,
          textString="W"),
        Text(
          extent={{-118,-60},{-38,-100}},
          lineColor={0,0,255},
          pattern=LinePattern.Dash,
          fillColor={224,237,255},
          fillPattern=FillPattern.Solid,
          textString="J W"),
        Text(
          extent={{30,-60},{110,-100}},
          lineColor={0,0,255},
          pattern=LinePattern.Dash,
          fillColor={224,237,255},
          fillPattern=FillPattern.Solid,
          textString="W J")}));
end EnergySensorsSinglePhase;
