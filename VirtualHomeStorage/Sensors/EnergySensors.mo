within VirtualHomeStorage.Sensors;
model EnergySensors "Analog sensor to determine positive, negative and total power and energy"
  extends Modelica.Icons.RoundSensor;
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor annotation (
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pv "Positive pin of voltage path"
                                                       annotation (
    Placement(visible = true, transformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin nc "Negative pin of current path"
                                                       annotation (
    Placement(visible = true, transformation(origin={100,0},   extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin={100,0},   extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin nv "Negative pin of voltage path"
                                                       annotation (
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Nonlinear.Limiter limiterPos(final uMax=Modelica.Constants.inf,
      final uMin=0) annotation (Placement(visible=true, transformation(
        origin={30,-30},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Blocks.Nonlinear.Limiter limiterNeg(final uMax=0, final uMin=-
        Modelica.Constants.inf) annotation (Placement(visible=true,
        transformation(
        origin={-30,-30},
        extent={{10,-10},{-10,10}},
        rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput powerPos "Positive power"
                                                 annotation (
    Placement(visible = true, transformation(origin={60,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={60,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput powerNeg "Negative power"
                                                 annotation (
    Placement(visible = true, transformation(origin={-60,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={-60,-110},    extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput energyNeg "Negative energy determined from negative power"
                                                  annotation (
    Placement(visible = true, transformation(origin={-100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={-100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput energyPos "Positive energy determined from positive power"
                                                  annotation (
    Placement(visible = true, transformation(origin={100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin={100,-110},   extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Continuous.Integrator integratorPos(final k=1)
                                                      annotation (
    Placement(visible = true, transformation(origin = {70, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.Integrator integratorNeg(final k=1) annotation (Placement(visible=true, transformation(
        origin={-70,-30},
        extent={{10,-10},{-10,10}},
        rotation=0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pc "Positive pin of current path"
                                                       annotation (
    Placement(visible = true, transformation(origin={-100,0},   extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin={-100,0},   extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput power "Positive and negative (total) power" annotation (Placement(
      visible=true,
      transformation(
        origin={-20,-110},
        extent={{-10,-10},{10,10}},
        rotation=-90),
      iconTransformation(
        origin={-110,-60},
        extent={{-10,-10},{10,10}},
        rotation=180)));
equation
  connect(powerSensor.pv, pv) annotation (
    Line(points = {{0, 10}, {0, 10}, {0, 100}, {0, 100}}, color = {0, 0, 255}));
  connect(powerSensor.nc, nc) annotation (
    Line(points={{10,0},{100,0}},     color = {0, 0, 255}));
  connect(powerSensor.nv, nv) annotation (
    Line(points = {{0, -10}, {0, -10}, {0, -100}, {0, -100}}, color = {0, 0, 255}));
  connect(powerSensor.power, limiterNeg.u) annotation (
    Line(points={{-10,-11},{-10,-11},{-10,-30},{-18,-30},{-18,-30}},            color = {0, 0, 127}));
  connect(powerSensor.power, limiterPos.u) annotation (
    Line(points={{-10,-11},{-10,-11},{-10,-30},{18,-30},{18,-30}},            color = {0, 0, 127}));
  connect(limiterNeg.y, powerNeg) annotation (
    Line(points={{-41,-30},{-48,-30},{-48,-80},{-60,-80},{-60,-110}},
                                                         color = {0, 0, 127}));
  connect(limiterPos.y, powerPos) annotation (
    Line(points={{41,-30},{50,-30},{50,-80},{60,-80},{60,-110}},                                    color = {0, 0, 127}));
  connect(limiterPos.y, integratorPos.u) annotation (
    Line(points={{41,-30},{58,-30}},      color = {0, 0, 127}));
  connect(integratorPos.y, energyPos) annotation (
    Line(points={{81,-30},{90,-30},{90,-80},{100,-80},{100,-110}},          color = {0, 0, 127}));
  connect(limiterNeg.y, integratorNeg.u) annotation (Line(points={{-41,-30},{-58,-30}}, color={0,0,127}));
  connect(integratorNeg.y, energyNeg) annotation (Line(points={{-81,-30},{-90,-30},{-90,-80},{-100,-80},{-100,-110}}, color={0,0,127}));
  connect(pc, powerSensor.pc) annotation (
    Line(points={{-100,0},{-10,0},{-10,0},{-10,0}},         color = {0, 0, 255}));
  connect(powerSensor.pc, pc) annotation (
    Line(points={{-10,0},{-10,0},{-10,0},{-100,0}},         color = {0, 0, 255}));
  connect(powerSensor.power, power) annotation (Line(points={{-10,-11},{-10,-80},{-20,-80},{-20,-110}}, color={0,0,127}));
  annotation (Icon(graphics={
        Line(points={{-40,-40},{-20,-40}}, color={0,0,0}),
        Line(points={{20,-40},{40,-40}}, color={0,0,0}),
        Line(points={{30,-30},{30,-50}}, color={0,0,0}),
        Line(
          points={{-100,-60},{-80,-60},{-58,-38}},
          color={0,0,0},
          pattern=LinePattern.Dash),           Text(
          extent={{-150,150},{150,110}},
          textString="%name",
          lineColor={0,0,255}),
        Text(
          extent={{-118,-60},{-38,-100}},
          lineColor={0,0,255},
          pattern=LinePattern.Dash,
          fillColor={224,237,255},
          fillPattern=FillPattern.Solid,
          textString="J W"),
        Text(
          extent={{30,-60},{110,-100}},
          lineColor={0,0,255},
          pattern=LinePattern.Dash,
          fillColor={224,237,255},
          fillPattern=FillPattern.Solid,
          textString="W J"),
        Text(
          extent={{-130,-10},{-70,-50}},
          lineColor={0,0,255},
          pattern=LinePattern.Dash,
          fillColor={224,237,255},
          fillPattern=FillPattern.Solid,
          textString="W")}));
end EnergySensors;
