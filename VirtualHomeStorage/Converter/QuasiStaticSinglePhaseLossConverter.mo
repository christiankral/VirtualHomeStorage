within VirtualHomeStorage.Converter;
model QuasiStaticSinglePhaseLossConverter "Quasi static single phase DC/AC converter with loss"
  extends Modelica.Electrical.PowerConverters.Interfaces.DCAC.DCtwoPin;
  extends .PhotoVoltaics.Interfaces.QuasiStatic.ACpins;
  extends .PhotoVoltaics.Icons.Converter;
  parameter Modelica.Units.SI.Voltage VRef=400/sqrt(3) "Reference voltage";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";
  parameter Modelica.Units.SI.Power table[:,2]=[0,10; 1000,50]
    "Output power vs loss table [...; output_power, loss_power; ...]";
  Modelica.Units.SI.Power powerDC=vDC*iDC "Power of DC side";
  Modelica.Units.SI.Power powerAC=Modelica.ComplexMath.real(vAC*
      Modelica.ComplexMath.conj(iAC)) "Complex apparent power of AC side";
  Modelica.Blocks.Interfaces.RealInput vDCRef(final unit = "V") "DC voltage" annotation (
    Placement(transformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin={60,-120}),   iconTransformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin={60,-120})));
  VirtualHomeStorage.Converter.Components.Loss efficiency annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  VirtualHomeStorage.Converter.Components.QuasiStaticSinglePhaseConverter                          converter(final VRef=VRef, final Ti=Ti) annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  Modelica.Blocks.Tables.CombiTable1Ds combiTable(final table=table, extrapolation=Modelica.Blocks.Types.Extrapolation.LastTwoPoints)
                                                                          annotation (Placement(transformation(extent={{-60,-50},{-40,-30}})));
Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(transformation(extent={{-30,-60},{-10,-40}})));
  Modelica.Blocks.Interfaces.RealOutput outputPower(unit="W") "Output power" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-110})));
equation
  connect(efficiency.outputPower, combiTable.u) annotation (Line(points={{-56,-11},{-56,-20},{-70,-20},{-70,-40},{-62,-40}}, color={0,0,127}));
  connect(combiTable.y[1], efficiency.lossPower) annotation (Line(points={{-39,-40},{-30,-40},{-30,-20},{-44,-20},{-44,-12}}, color={0,0,127}));
  connect(dc_p, efficiency.p1) annotation (Line(points={{-100,60},{-80,60},{-80,10},{-60,10}}, color={0,0,255}));
  connect(dc_n, efficiency.n1) annotation (Line(points={{-100,-60},{-80,-60},{-80,-10},{-60,-10}}, color={0,0,255}));
  connect(efficiency.p2, converter.dc_p) annotation (Line(points={{-40,10},{-20,10},{-20,6},{0,6}}, color={0,0,255}));
  connect(efficiency.n2, converter.dc_n) annotation (Line(points={{-40,-10},{-20,-10},{-20,-6},{0,-6}}, color={0,0,255}));
  connect(converter.ac_p, ac_p) annotation (Line(points={{20,6},{80,6},{80,60},{100,60}},     color={85,170,255}));
  connect(converter.ac_n, ac_n) annotation (Line(points={{20,-6},{80,-6},{80,-60},{100,-60}},     color={85,170,255}));
  connect(vDCRef, converter.vDCRef) annotation (Line(points={{60,-120},{60,-80},{4,-80},{4,-12}},   color={0,0,127}));
connect(ground.p, converter.dc_n) annotation (Line(points={{-20,-40},{-20,-6},{0,-6}}, color={0,0,255}));
connect(efficiency.outputPower, outputPower) annotation (Line(points={{-56,-11},{-56,-20},{-70,-20},{-70,-70},{0,-70},{0,-110}}, color={0,0,127}));
  annotation (
    defaultComponentName = "converter",
    Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={                                                                                                                                                                 Text(extent = {{-100, 40}, {-40, -40}}, lineColor = {0, 0, 255}, textString = "="), Text(extent = {{40, 40}, {100, -40}}, lineColor = {0, 0, 255}, textString = "~"), Text(extent = {{-150, 150}, {150, 110}}, lineColor = {0, 0, 255}, textString = "%name"), Text(extent={{-152,-112},{-92,-152}},   lineColor = {0, 0, 255}, pattern = LinePattern.Dash, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, textString = "vDCRef"), Text(extent={{-80,90},{20,50}},      lineColor={0,0,255},     pattern=LinePattern.Dash,   fillColor={0,0,255},     fillPattern=FillPattern.Solid,
          textString="PV"),                                                                                                                                                                                                        Text(extent={{-40,-50},{60,-90}},      lineColor = {0, 0, 255}, pattern = LinePattern.Dash, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, textString = "src"),
      Polygon(
        points={{-40,10},{0,10},{0,22},{40,0},{0,-22},{0,-10},{-14,-10},{-14,-30},{-4,-30},{-20,-46},{-36,-30},{-26,-30},{-26,-22},{-40,-22},{-40,10}},
        lineColor={0,0,0},
        fillColor={192,0,0},
        fillPattern=FillPattern.Solid)}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}})),
    Documentation(info="<html>
<p>This is an ideal DC/AC converter.</p>
<p>
The DC/AC converter is characterized by:
</p> 
<ul>
<li>Losses are not considered</li> 
<li>The AC output current is determined based on power balance, calculating with instantanteous DC and complex AC phasor values: 
    <code>vDC*iDC + real(
    </code><u><code>v</code></u><code>AC*</code><u><code>i</code></u><code>AC</code><sup><code>*</code></sup> 
    <code>)= 0</code></li>
<<li>The DC input voltage <code>vDCRef</code> is applied to the DC side without limitations</li>
</ul>
</html>"));
end QuasiStaticSinglePhaseLossConverter;
