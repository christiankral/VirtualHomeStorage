within VirtualHomeStorage.Converter.Components;
model QuasiStaticSinglePhaseConverter "Ideal quasi static single phase DC/AC converter"
  extends Modelica.Electrical.PowerConverters.Interfaces.DCAC.DCtwoPin;
  extends .PhotoVoltaics.Interfaces.QuasiStatic.ACpins;
  extends .PhotoVoltaics.Icons.Converter;
  parameter Modelica.Units.SI.Voltage VRef=400/sqrt(3) "Reference voltage";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";
  Modelica.Units.SI.Power powerDC=vDC*iDC "Power of DC side";
  Modelica.Units.SI.Power powerAC=Modelica.ComplexMath.real(vAC*
      Modelica.ComplexMath.conj(iAC)) "Complex apparent power of AC side";
  Modelica.Blocks.Interfaces.RealInput vDCRef(final unit = "V") "DC voltage" annotation (
    Placement(transformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin={-60,-120}),  iconTransformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin={-60,-120})));
  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin={-80,-20})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (
    Placement(transformation(extent = {{-10, 10}, {10, -10}}, rotation = 270, origin={-80,30})));
  Modelica.Blocks.Math.Product product annotation (
    Placement(transformation(extent={{-30,-10},{-10,10}})));
  Modelica.Blocks.Math.Gain gain(final k=-1) annotation (Placement(transformation(extent={{8,-10},{28,10}})));
  Loads.SinglePhaseLoad load annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={60,0})));
equation
  connect(currentSensor.n,signalVoltage. p) annotation (Line(points={{-80,20},{-80,-10}},                color = {0, 0, 255}));
  connect(currentSensor.i,product. u1) annotation (Line(points={{-69,30},{-50,30},{-50,6},{-32,6}},                       color = {0, 0, 127}));
  connect(vDCRef, product.u2) annotation (Line(points={{-60,-120},{-60,-80},{-50,-80},{-50,-6},{-32,-6}}, color={0,0,127}));
  connect(vDCRef, signalVoltage.v) annotation (Line(points={{-60,-120},{-60,-80},{-50,-80},{-50,-20},{-68,-20}}, color={0,0,127}));
  connect(product.y, gain.u) annotation (Line(points={{-9,0},{6,0}}, color={0,0,127}));
  connect(gain.y, load.power) annotation (Line(points={{29,0},{48,0}}, color={0,0,127}));
  connect(load.pin_p, ac_p) annotation (Line(points={{60,10},{60,100},{100,100}}, color={85,170,255}));
  connect(load.pin_n, ac_n) annotation (Line(points={{60,-10},{60,-100},{100,-100}}, color={85,170,255}));
  connect(currentSensor.p, dc_p) annotation (Line(points={{-80,40},{-80,60},{-100,60}}, color={0,0,255}));
  connect(signalVoltage.n, dc_n) annotation (Line(points={{-80,-30},{-80,-60},{-100,-60}}, color={0,0,255}));
  annotation (
    defaultComponentName = "converter",
    Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={                                                                                                                                                                 Text(extent = {{-100, 40}, {-40, -40}}, lineColor = {0, 0, 255}, textString = "="), Text(extent = {{40, 40}, {100, -40}}, lineColor = {0, 0, 255}, textString = "~"), Text(extent = {{-150, 150}, {150, 110}}, lineColor = {0, 0, 255}, textString = "%name"), Text(extent={{-152,-112},{-92,-152}},   lineColor = {0, 0, 255}, pattern = LinePattern.Dash, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, textString = "vDCRef"), Text(extent={{-80,90},{20,50}},      lineColor={0,0,255},     pattern=LinePattern.Dash,   fillColor={0,0,255},     fillPattern=FillPattern.Solid,
          textString="PV"),                                                                                                                                                                                                        Text(extent={{-40,-50},{60,-90}},      lineColor = {0, 0, 255}, pattern = LinePattern.Dash, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, textString = "src")}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}})),
    Documentation(info="<html>
<p>This is an ideal DC/AC converter.</p>
<p>
The DC/AC converter is characterized by:
</p> 
<ul>
<li>Losses are not considered</li> 
<li>The AC output current is determined based on power balance, calculating with instantanteous DC and complex AC phasor values: 
    <code>vDC*iDC + real(
    </code><u><code>v</code></u><code>AC*</code><u><code>i</code></u><code>AC</code><sup><code>*</code></sup> 
    <code>)= 0</code></li>
<<li>The DC input voltage <code>vDCRef</code> is applied to the DC side without limitations</li>
</ul>
</html>"));
end QuasiStaticSinglePhaseConverter;
