within VirtualHomeStorage.Converter.Components;
model ConstantLoss "Constant loss model"
  extends Modelica.Electrical.Analog.Interfaces.TwoPort;
  parameter Modelica.Units.SI.Power lossPower=100 "Loss power";
  Modelica.Units.SI.Current i_loss
    "Loss current considering equal voltages on both sides";
equation
  // Equal voltages on both sides
  v1 = v2;
  // Currents take loss into account
  i1 + i2 = smooth(0, if v1 * i1 > 0  then i_loss else -i_loss);
  i_loss * v1 = lossPower;
  annotation(defaultComponentName="efficiency",Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Rectangle(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0},
          fillPattern =                                                                                                   FillPattern.Solid, fillColor = {255, 255, 255}), Text(extent = {{-150, 150}, {150, 110}}, textString = "%name", lineColor = {0, 0, 255}),
        Polygon(
          points={{-68,50},{12,50},{12,70},{72,20},{12,-30},{12,-10},{-28,-10},{-28,-50},{-18,-50},{-38,-70},{-58,-50},{-48,-50},{-48,-30},{-68,-30},{-68,50}},
          lineColor={0,0,0},
          fillColor={192,0,0},
          fillPattern=FillPattern.Solid)}),                                                                                                                                                      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics),
    Documentation(info="<html>
<p>Model of constant loss independent voltage and current.</p>
</html>"));
end ConstantLoss;
