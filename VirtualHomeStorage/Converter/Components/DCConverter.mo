within VirtualHomeStorage.Converter.Components;
model DCConverter "DC controlled single phase DC/AC converter"
  extends Modelica.Electrical.PowerConverters.Interfaces.DCDC.DCtwoPin1;
  extends Modelica.Electrical.PowerConverters.Interfaces.DCDC.DCtwoPin2;
  extends .PhotoVoltaics.Icons.Converter;

  parameter Modelica.Units.SI.Voltage VRef=48 "Reference DC source voltage";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";
  Modelica.Blocks.Interfaces.RealInput vDCRef(final unit = "V") "DC voltage" annotation (
    Placement(transformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin={-60,-120}),  iconTransformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin={-60,-120})));
  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin={-80,-20})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (
    Placement(transformation(extent = {{-10, 10}, {10, -10}}, rotation = 270, origin={-80,30})));
  Modelica.Blocks.Math.Product product annotation (
    Placement(transformation(extent={{-30,-10},{-10,10}})));

  Loads.AnalogLoad load(final VRef=VRef, final Ti=Ti) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={60,0})));
  Modelica.Blocks.Math.Gain gain(final k=-1) annotation (Placement(transformation(extent={{8,-10},{28,10}})));
equation
  connect(currentSensor.n, signalVoltage.p) annotation (Line(points={{-80,20},{-80,-10}},                color = {0, 0, 255}));
  connect(currentSensor.i, product.u1) annotation (Line(points={{-69,30},{-50,30},{-50,6},{-32,6}},                       color = {0, 0, 127}));
  connect(currentSensor.p, dc_p1) annotation (Line(points={{-80,40},{-80,60},{-100,60}},          color={0,0,255}));
  connect(signalVoltage.n, dc_n1) annotation (Line(points={{-80,-30},{-80,-60},{-100,-60}},           color={0,0,255}));
  connect(load.p, dc_p2) annotation (Line(points={{60,10},{60,60},{100,60}}, color={0,0,255}));
  connect(load.n, dc_n2) annotation (Line(points={{60,-10},{60,-60},{100,-60}}, color={0,0,255}));
  connect(vDCRef, product.u2) annotation (Line(points={{-60,-120},{-60,-80},{-50,-80},{-50,-6},{-32,-6}}, color={0,0,127}));
  connect(vDCRef, signalVoltage.v) annotation (Line(points={{-60,-120},{-60,-80},{-50,-80},{-50,-20},{-68,-20}}, color={0,0,127}));
  connect(product.y, gain.u) annotation (Line(points={{-9,0},{6,0}}, color={0,0,127}));
  connect(gain.y, load.power) annotation (Line(points={{29,0},{38.5,0},{38.5,2.22045e-15},{48,2.22045e-15}}, color={0,0,127}));
  annotation (
    defaultComponentName = "converter",
    Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={                                                                                                                                                                 Text(extent={{-100,40},{-40,-40}},      lineColor = {0, 0, 255}, textString = "="),                                                                                   Text(extent = {{-150, 150}, {150, 110}}, lineColor = {0, 0, 255}, textString = "%name"), Text(extent={{-150,-110},{-90,-150}},   lineColor = {0, 0, 255}, pattern = LinePattern.Dash, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, textString = "vDCRef"), Text(extent={{-80,90},{20,50}},      lineColor={0,0,255},     pattern=LinePattern.Dash,   fillColor={0,0,255},     fillPattern=FillPattern.Solid,
          textString="PV"),                                                                                                                                                                                                        Text(extent={{-40,-50},{60,-90}},      lineColor={0,0,255},     pattern=LinePattern.Dash,   fillColor={0,0,255},     fillPattern=
              FillPattern.Solid,
          textString="bat"),                                                                                                                                                                                                        Text(extent={{40,40},{100,-40}},        lineColor = {0, 0, 255}, textString = "=")}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}})),
    Documentation(info="<html>
<p>This is an ideal DC/DC converter.</p>
<p>
The DC/DC converter is characterized by:
</p> 
<ul>
<li>Losses are not considered</li> 
<li>The AC output current is determined based on power balance, calculating with instantaneous values: 
    <code>vDC1*iDC1 + vDC2*iDC2 = 0<code></li>
<li>The DC input voltage <code>vDCRef</code> is applied to the DC side 1 without limitations</li>
<li>The phase angle input <code>phi</code> influences the AC reactive power based on the following figure,
    where underlined voltages and currents represent complex phasors</li>
</ul>
</html>"));
end DCConverter;
