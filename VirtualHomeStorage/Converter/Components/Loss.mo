within VirtualHomeStorage.Converter.Components;
model Loss "Efficiency model considering variable efficiency"
  extends Modelica.Electrical.Analog.Interfaces.TwoPort;
  Modelica.Units.SI.Current i_loss
    "Loss current considering equal voltages on both sides";
  Modelica.Blocks.Interfaces.RealInput lossPower(unit="W") "Loss power" annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={60,-120})));
  Modelica.Blocks.Interfaces.RealOutput outputPower(unit="W") "Output power" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-60,-110})));
equation
  // Equal voltages on both sides
  v1 = v2;
  // Currents take loss into account
  i1 + i2 = i_loss;
  i_loss * v1 = lossPower;
  outputPower = smooth(0, noEvent(if v1*i1 < 0 then -v1*i1 else -v2*i2));
  annotation(defaultComponentName="efficiency",Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Rectangle(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0},
          fillPattern =                                                                                                   FillPattern.Solid, fillColor = {255, 255, 255}), Text(extent = {{-150, 150}, {150, 110}}, textString = "%name", lineColor = {0, 0, 255}),
        Polygon(
          points={{-68,50},{12,50},{12,70},{72,20},{12,-30},{12,-10},{-28,-10},{-28,-50},{-18,-50},{-38,-70},{-58,-50},{-48,-50},{-48,-30},{-68,-30},{-68,50}},
          lineColor={0,0,0},
          fillColor={192,0,0},
          fillPattern=FillPattern.Solid)}),                                                                                                                                                      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}})),
    Documentation(info="<html>
<p>Model of variable loss with signal output representing output power.</p>
</html>"));
end Loss;
