within VirtualHomeStorage.Converter;
model QuasiStaticSinglePhaseLossConverterControlled "Controlled quasi static single phase DC/AC converter with loss"
  extends Modelica.Electrical.PowerConverters.Interfaces.DCAC.DCtwoPin;
  extends .PhotoVoltaics.Interfaces.QuasiStatic.ACpins;
  extends .PhotoVoltaics.Icons.Converter;
  parameter Modelica.Units.SI.Voltage VRef=400/sqrt(3) "Reference voltage";
  parameter Modelica.Units.SI.Power PRef=5000 "Reference power (> 0)";
  final parameter Modelica.Units.SI.Current IRef=PRef/VRef "Reference current";
  parameter Modelica.Units.SI.Time Ti=1E-6 "Internal integration time constant";
  parameter Modelica.Units.SI.Time Tc=1E-6
    "Internal power control integration time constant";
  parameter Modelica.Units.SI.Power table[:,2]=[0,10; 1000,50]
    "Output power vs loss table [...; output_power, loss_power; ...]";
  parameter Modelica.Units.SI.Power power_start=0 "Start value of output power";
  Modelica.Units.SI.Power powerDC=vDC*iDC "Power of DC side";
  Modelica.Units.SI.Power powerAC=Modelica.ComplexMath.real(vAC*
      Modelica.ComplexMath.conj(iAC)) "Complex apparent power of AC side";
  Modelica.Blocks.Interfaces.RealInput powerRef(final unit = "W") "Reference output power to be controlled" annotation (
    Placement(transformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin = {60, -120}), iconTransformation(extent = {{-20, -20}, {20, 20}}, rotation = 90, origin = {60, -120})));
  Modelica.Blocks.Interfaces.RealOutput outputPower(unit="W") "Output power" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-110})));
  VirtualHomeStorage.Converter.QuasiStaticSinglePhaseLossConverter converter(
    final VRef=VRef,
    final Ti=Ti,
    final table=table) annotation (Placement(transformation(extent={{-40,60},{-20,80}})));
  Modelica.Blocks.Math.Feedback feedback annotation (Placement(transformation(extent={{46,-50},{26,-70}})));
  Modelica.Blocks.Continuous.LimIntegrator
                                        controller(
    k=-1/IRef/Tc,
  outMax=PRef,
    y_start=4.0625*9,
    y(unit="V"))                                                       annotation (Placement(transformation(extent={{10,-70},{-10,-50}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Sensors.PowerSensor powerSensorAC
    annotation (Placement(transformation(extent={{30,70},{50,90}})));
  Modelica.ComplexBlocks.ComplexMath.ComplexToReal complexToReal annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,40})));
  Modelica.Blocks.Math.Gain gain(final k=-1) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,-80})));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensorDC annotation (Placement(transformation(extent={{-70,70},{-50,90}})));
equation
  connect(converter.outputPower, outputPower) annotation (Line(points={{-30,59},{-30,-92},{0,-92},{0,-110}},                 color={0,0,127}));
  connect(dc_n, converter.dc_n) annotation (Line(points={{-100,-60},{-80,-60},{-80,64},{-40,64}}, color={0,0,255}));
  connect(converter.ac_n, ac_n) annotation (Line(points={{-20,64},{80,64},{80,-60},{100,-60}},  color={85,170,255}));
  connect(feedback.y, controller.u) annotation (Line(points={{27,-60},{12,-60}}, color={0,0,127}));
  connect(controller.y, converter.vDCRef) annotation (Line(points={{-11,-60},{-24,-60},{-24,58}},               color={0,0,127}));
  connect(powerSensorAC.voltageP, powerSensorAC.currentP) annotation (Line(points={{40,90},{30,90},{30,80}}, color={85,170,255}));
  connect(powerSensorAC.voltageN, ac_n) annotation (Line(points={{40,70},{40,60},{80,60},{80,-60},{100,-60}},   color={85,170,255}));
  connect(complexToReal.u, powerSensorAC.apparentPower)
    annotation (Line(points={{30,52},{30,69}}, color={85,170,255}));
  connect(powerSensorAC.currentP, converter.ac_p) annotation (Line(points={{30,80},{6,80},{6,76},{-20,76}},
                                                                                              color={85,170,255}));
  connect(powerSensorAC.currentN, ac_p) annotation (Line(points={{50,80},{80,80},{80,60},{100,60}},   color={85,170,255}));
  connect(gain.u, powerRef) annotation (Line(points={{60,-92},{60,-120},{60,-120}}, color={0,0,127}));
  connect(gain.y, feedback.u1) annotation (Line(points={{60,-69},{60,-60},{44,-60}}, color={0,0,127}));
  connect(dc_p, powerSensorDC.pc) annotation (Line(points={{-100,60},{-100,80},{-70,80}}, color={0,0,255}));
  connect(powerSensorDC.nc, converter.dc_p) annotation (Line(points={{-50,80},{-50,76},{-40,76}}, color={0,0,255}));
  connect(powerSensorDC.nv, converter.dc_n) annotation (Line(points={{-60,70},{-60,64},{-40,64}}, color={0,0,255}));
  connect(powerSensorDC.power, feedback.u2) annotation (Line(points={{-70,69},{-70,0},{36,0},{36,-52}}, color={0,0,127}));
  connect(powerSensorDC.pc, powerSensorDC.pv) annotation (Line(points={{-70,80},{-70,90},{-60,90}}, color={0,0,255}));
  annotation (
    defaultComponentName = "converter",
    Icon(coordinateSystem(preserveAspectRatio = false, initialScale = 0.1), graphics={  Text(lineColor = {0, 0, 255}, extent = {{-100, 40}, {-40, -40}}, textString = "="), Text(lineColor = {0, 0, 255}, extent = {{40, 40}, {100, -40}}, textString = "~"), Text(lineColor = {0, 0, 255}, extent = {{-150, 150}, {150, 110}}, textString = "%name"),                                                                                                                                                                                   Text(lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-80, 90}, {20, 50}}, textString = "PV"), Text(lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, pattern = LinePattern.Dash,
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-40, -50}, {60, -90}}, textString = "src"), Polygon(fillColor = {192, 0, 0},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, points = {{-40, 10}, {0, 10}, {0, 22}, {40, 0}, {0, -22}, {0, -10}, {-14, -10}, {-14, -30}, {-4, -30}, {-20, -46}, {-36, -30}, {-26, -30}, {-26, -22}, {-40, -22}, {-40, 10}})}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}})),
    Documentation(info = "<html>
<p>This is an ideal DC/AC converter.</p>
<p>
The DC/AC converter is characterized by:
</p> 
<ul>
<li>Losses are not considered</li> 
<li>The AC output current is determined based on power balance, calculating with instantanteous DC and complex AC phasor values: 
    <code>vDC*iDC + real(
    </code><u><code>v</code></u><code>AC*</code><u><code>i</code></u><code>AC</code><sup><code>*</code></sup> 
    <code>)= 0</code></li>
<<li>The DC input voltage <code>vDCRef</code> is applied to the DC side without limitations</li>
</ul>
</html>"));
end QuasiStaticSinglePhaseLossConverterControlled;
