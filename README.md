# VirtualHomeStorage

![VirtualHomeStorage icon](VirtualHomeStorage/Resources/Icon/VirtualHomeStorage.png)

Modelica library for the investigation of real and virtual electrical home storages of homes utilizing photovoltaics. This library ist based on the open source library [PhotoVoltaics](https://github.com/christiankral/PhotoVoltaics).

![Simualation example](VirtualHomeStorage/Resources/Images/eyecatcher.png)